package de.fuchspfoten.sacknaht.test;

import de.fuchspfoten.sacknaht.model.ActionOutcome;
import de.fuchspfoten.sacknaht.model.ActionOutcome.ActionResult;
import de.fuchspfoten.sacknaht.model.CellType;
import de.fuchspfoten.sacknaht.model.CellView;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Regression tests for cell bugs.
 */
public class CellBugRegressionTest extends SacknahtTest {

    @Test
    public void regression_BackpackDuplicateOutOfSync() {
        final ItemPattern i1 = new ItemPattern(new ItemStack(Material.WOOD, 1, (short) 2));
        final ItemPattern i2 = new ItemPattern(new ItemStack(Material.PURPUR_BLOCK));
        final ItemPattern i3 = new ItemPattern(new ItemStack(Material.PURPUR_PILLAR));
        final ItemPattern i4 = new ItemPattern(new ItemStack(Material.PURPUR_STAIRS));
        final ItemPattern i5 = new ItemPattern(new ItemStack(Material.PURPUR_SLAB));

        final UUID owner = UUID.randomUUID();
        final StorageCell sc = plugin.getCellManager().getNewCell(owner, 5, 576, CellType.BACKPACK);
        final CellView view = new CellView(Collections.singleton(sc), null);

        final ActionOutcome out1 = view.addItem(i1, 64);
        Assert.assertEquals(out1.getResult(), ActionResult.OK);
        Assert.assertNull(out1.getOutcome());
        Assert.assertEquals(1, view.getUsedSlots());
        Assert.assertEquals(1, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i1));
        final List<ItemStack> firstPage1 = view.getContentPart(45, 0);
        Assert.assertEquals(1, firstPage1.size());

        final ActionOutcome out2 = view.addItem(i2, 64);
        Assert.assertEquals(out2.getResult(), ActionResult.OK);
        Assert.assertNull(out2.getOutcome());
        Assert.assertEquals(2, view.getUsedSlots());
        Assert.assertEquals(2, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i2));
        final List<ItemStack> firstPage2 = view.getContentPart(45, 0);
        Assert.assertEquals(2, firstPage2.size());

        final ActionOutcome out3 = view.addItem(i3, 64);
        Assert.assertEquals(out3.getResult(), ActionResult.OK);
        Assert.assertNull(out3.getOutcome());
        Assert.assertEquals(3, view.getUsedSlots());
        Assert.assertEquals(3, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i3));
        final List<ItemStack> firstPage3 = view.getContentPart(45, 0);
        Assert.assertEquals(3, firstPage3.size());

        final ActionOutcome out4 = view.addItem(i4, 64);
        Assert.assertEquals(out4.getResult(), ActionResult.OK);
        Assert.assertNull(out4.getOutcome());
        Assert.assertEquals(4, view.getUsedSlots());
        Assert.assertEquals(4, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i4));
        final List<ItemStack> firstPage4 = view.getContentPart(45, 0);
        Assert.assertEquals(4, firstPage4.size());

        final ActionOutcome out5 = view.addItem(i5, 64);
        Assert.assertEquals(out5.getResult(), ActionResult.OK);
        Assert.assertNull(out5.getOutcome());
        Assert.assertEquals(5, view.getUsedSlots());
        Assert.assertEquals(5, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i5));
        final List<ItemStack> firstPage5 = view.getContentPart(45, 0);
        Assert.assertEquals(5, firstPage5.size());

        final long taken1 = view.take(i5, 64);
        Assert.assertEquals(64, taken1);
        Assert.assertEquals(4, view.getUsedSlots());
        Assert.assertEquals(4, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i5));
        final List<ItemStack> firstPage6 = view.getContentPart(45, 0);
        Assert.assertEquals(4, firstPage6.size());

        final long taken2 = view.take(i4, 64);
        Assert.assertEquals(64, taken2);
        Assert.assertEquals(3, view.getUsedSlots());
        Assert.assertEquals(3, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i4));
        final List<ItemStack> firstPage7 = view.getContentPart(45, 0);
        Assert.assertEquals(3, firstPage7.size());
    }

    @Test
    public void regression_BackpackDuplicateOutOfSync2() {
        final ItemPattern i1 = new ItemPattern(new ItemStack(Material.WOOD, 1, (short) 2));
        final ItemPattern i2 = new ItemPattern(new ItemStack(Material.PURPUR_BLOCK));
        final ItemPattern i3 = new ItemPattern(new ItemStack(Material.PURPUR_PILLAR));
        final ItemPattern i4 = new ItemPattern(new ItemStack(Material.PURPUR_STAIRS));
        final ItemPattern i5 = new ItemPattern(new ItemStack(Material.PURPUR_SLAB));

        final UUID owner = UUID.randomUUID();
        final StorageCell sc = plugin.getCellManager().getNewCell(owner, 5, 576, CellType.BACKPACK);
        final CellView view = new CellView(Collections.singleton(sc), null);

        final ActionOutcome out1 = view.addItem(i1, 64);
        Assert.assertEquals(out1.getResult(), ActionResult.OK);
        Assert.assertNull(out1.getOutcome());
        Assert.assertEquals(1, view.getUsedSlots());
        Assert.assertEquals(1, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i1));
        final List<ItemStack> firstPage1 = view.getContentPart(45, 0);
        Assert.assertEquals(1, firstPage1.size());

        final ActionOutcome out2 = view.addItem(i2, 64);
        Assert.assertEquals(out2.getResult(), ActionResult.OK);
        Assert.assertNull(out2.getOutcome());
        Assert.assertEquals(2, view.getUsedSlots());
        Assert.assertEquals(2, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i2));
        final List<ItemStack> firstPage2 = view.getContentPart(45, 0);
        Assert.assertEquals(2, firstPage2.size());

        final ActionOutcome out3 = view.addItem(i3, 64);
        Assert.assertEquals(out3.getResult(), ActionResult.OK);
        Assert.assertNull(out3.getOutcome());
        Assert.assertEquals(3, view.getUsedSlots());
        Assert.assertEquals(3, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i3));
        final List<ItemStack> firstPage3 = view.getContentPart(45, 0);
        Assert.assertEquals(3, firstPage3.size());

        final ActionOutcome out4 = view.addItem(i4, 64);
        Assert.assertEquals(out4.getResult(), ActionResult.OK);
        Assert.assertNull(out4.getOutcome());
        Assert.assertEquals(4, view.getUsedSlots());
        Assert.assertEquals(4, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i4));
        final List<ItemStack> firstPage4 = view.getContentPart(45, 0);
        Assert.assertEquals(4, firstPage4.size());

        final ActionOutcome out5 = view.addItem(i5, 64);
        Assert.assertEquals(out5.getResult(), ActionResult.OK);
        Assert.assertNull(out5.getOutcome());
        Assert.assertEquals(5, view.getUsedSlots());
        Assert.assertEquals(5, view.getRealUsedSlots());
        Assert.assertEquals(64, view.getAmountOf(i5));
        final List<ItemStack> firstPage5 = view.getContentPart(45, 0);
        Assert.assertEquals(5, firstPage5.size());

        final long taken1 = view.take(i5, 64);
        Assert.assertEquals(64, taken1);
        Assert.assertEquals(4, view.getUsedSlots());
        Assert.assertEquals(4, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i5));
        final List<ItemStack> firstPage6 = view.getContentPart(45, 0);
        Assert.assertEquals(4, firstPage6.size());

        final long taken2 = view.take(i4, 64);
        Assert.assertEquals(64, taken2);
        Assert.assertEquals(3, view.getUsedSlots());
        Assert.assertEquals(3, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i4));
        final List<ItemStack> firstPage7 = view.getContentPart(45, 0);
        Assert.assertEquals(3, firstPage7.size());

        final long taken3 = view.take(i3, 64);
        Assert.assertEquals(64, taken3);
        Assert.assertEquals(2, view.getUsedSlots());
        Assert.assertEquals(2, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i3));
        final List<ItemStack> firstPage8 = view.getContentPart(45, 0);
        Assert.assertEquals(2, firstPage8.size());

        final long taken4 = view.take(i2, 64);
        Assert.assertEquals(64, taken4);
        Assert.assertEquals(1, view.getUsedSlots());
        Assert.assertEquals(1, view.getRealUsedSlots());
        Assert.assertEquals(0, view.getAmountOf(i2));
        final List<ItemStack> firstPage9 = view.getContentPart(45, 0);
        Assert.assertEquals(1, firstPage9.size());
    }
}
