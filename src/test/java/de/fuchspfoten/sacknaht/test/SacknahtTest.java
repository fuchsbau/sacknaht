package de.fuchspfoten.sacknaht.test;

import de.fuchspfoten.fuchslib.FuchsLibPlugin;
import de.fuchspfoten.fuchslib.tests.BasePluginTest;
import de.fuchspfoten.sacknaht.SacknahtPlugin;

/**
 * Adapter for plugin tests.
 */
public abstract class SacknahtTest extends BasePluginTest<SacknahtPlugin> {

    /**
     * Constructor.
     */
    protected SacknahtTest() {
        super(SacknahtPlugin.class, FuchsLibPlugin.class);
    }
}
