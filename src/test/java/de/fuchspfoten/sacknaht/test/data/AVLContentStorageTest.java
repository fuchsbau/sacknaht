package de.fuchspfoten.sacknaht.test.data;

import de.fuchspfoten.sacknaht.data.AVLContentStorage;
import de.fuchspfoten.sacknaht.data.ContentStorage;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.test.SacknahtTest;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for {@link de.fuchspfoten.sacknaht.data.AVLContentStorage}.
 */
public class AVLContentStorageTest extends SacknahtTest {

    @Test
    public void addTwo() {
        final ItemPattern stone = new ItemPattern(new ItemStack(Material.STONE));
        final ItemPattern log = new ItemPattern(new ItemStack(Material.LOG));

        final ContentStorage store = new AVLContentStorage();
        Assert.assertEquals(0, store.size());
        store.put(log, 5);
        Assert.assertEquals(1, store.size());

        Assert.assertNull(store.get(stone));
        Assert.assertEquals(store.get(log).getPattern(), log);
        Assert.assertEquals(store.get(log).getAmount(), 5);
    }

    @Test
    public void empty() {
        final ContentStorage store = new AVLContentStorage();
        Assert.assertEquals(0, store.size());

        final ItemPattern pattern = new ItemPattern(new ItemStack(Material.STONE));
        Assert.assertNull(store.get(pattern));
    }

    @Test
    public void regression_MergeRecord11_12() {
        final ItemPattern r11 = new ItemPattern(new ItemStack(Material.RECORD_11));
        final ItemPattern r12 = new ItemPattern(new ItemStack(Material.RECORD_12));
        final ContentStorage store = new AVLContentStorage();
        Assert.assertEquals(0, store.size());
        store.put(r11, 1);
        Assert.assertEquals(1, store.size());
        store.put(r12, 1);
        Assert.assertEquals(2, store.size());
        Assert.assertEquals(store.get(r11).getPattern(), r11);
        Assert.assertEquals(store.get(r11).getAmount(), 1);
        Assert.assertEquals(store.get(r12).getPattern(), r12);
        Assert.assertEquals(store.get(r12).getAmount(), 1);

        final ContentStorage target = new AVLContentStorage();
        Assert.assertEquals(0, target.size());
        target.merge(store);
        Assert.assertEquals(2, target.size());
        Assert.assertEquals(target.get(r11).getPattern(), r11);
        Assert.assertEquals(target.get(r11).getAmount(), 1);
        Assert.assertEquals(target.get(r12).getPattern(), r12);
        Assert.assertEquals(target.get(r12).getAmount(), 1);
    }

    @Test
    public void regression_MergeRecord5() {
        final ItemPattern r5 = new ItemPattern(new ItemStack(Material.RECORD_5));
        final ContentStorage store = new AVLContentStorage();
        Assert.assertEquals(0, store.size());
        store.put(r5, 1);
        Assert.assertEquals(1, store.size());
        Assert.assertEquals(store.get(r5).getPattern(), r5);
        Assert.assertEquals(store.get(r5).getAmount(), 1);

        final ContentStorage target = new AVLContentStorage();
        Assert.assertEquals(0, target.size());
        target.merge(store);
        Assert.assertEquals(1, target.size());
        Assert.assertEquals(target.get(r5).getPattern(), r5);
        Assert.assertEquals(target.get(r5).getAmount(), 1);
    }
}
