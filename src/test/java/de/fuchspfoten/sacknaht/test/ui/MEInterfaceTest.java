package de.fuchspfoten.sacknaht.test.ui;

import de.fuchspfoten.fuchslib.tests.TestHelper;
import de.fuchspfoten.mokkit.CancelledByEventException;
import de.fuchspfoten.mokkit.entity.living.human.MokkitPlayer;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.CellType;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.model.StorageCell;
import de.fuchspfoten.sacknaht.test.SacknahtTest;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * A test for the {@link de.fuchspfoten.sacknaht.ui.MEInterface}.
 */
public class MEInterfaceTest extends SacknahtTest {

    /**
     * A player.
     */
    private MokkitPlayer player;

    /**
     * A storage cell of the player.
     */
    private StorageCell cell;

    @Before
    public void prepare() {
        player = server.mokkit().joinPlayer("player");
        cell = SacknahtPlugin.getSelf().getCellManager().getNewCell(player.getUniqueId(), 5, 576,
                CellType.BACKPACK);
        player.getInventory().setItemInMainHand(cell.getItem());
    }

    @Test
    public void testOpen() {
        player.mokkit().rightClickBlock(player.getLocation().getBlock(), BlockFace.UP);

        Assert.assertNotNull(player.getOpenInventory());
        Assert.assertTrue(SacknahtPlugin.getSelf().getInterfaceManager().isInInterface(player.getUniqueId()));

        final Inventory ui = player.getOpenInventory().getTopInventory();

        // Toolbar.
        Assert.assertEquals(ui.getItem(45).getType(), Material.DIAMOND_HOE);
        // No left page.
        Assert.assertEquals(ui.getItem(49).getType(), Material.DIAMOND_HOE);
        // No right page.
        Assert.assertEquals(ui.getItem(53).getType(), Material.DIAMOND_HOE);
    }

    @Test
    public void testAdd() {
        player.mokkit().rightClickBlock(player.getLocation().getBlock(), BlockFace.UP);
        Assert.assertNotNull(player.getOpenInventory());
        Assert.assertTrue(SacknahtPlugin.getSelf().getInterfaceManager().isInInterface(player.getUniqueId()));

        final ItemPattern diamond = new ItemPattern(new ItemStack(Material.DIAMOND));
        player.getInventory().setItem(10, new ItemStack(Material.DIAMOND, 16));

        Assert.assertEquals(0, cell.getUsedSlots());
        Assert.assertEquals(0, cell.getRealUsedSlots());
        Assert.assertEquals(0, cell.getAmountOf(diamond));

        // Add the item.
        player.mokkit().clickInBottomInventory(10, ClickType.LEFT, 0);
        TestHelper.assertThrows(() -> player.mokkit().clickInTopInventory(0, ClickType.LEFT, 0),
                CancelledByEventException.class);

        Assert.assertEquals(1, cell.getUsedSlots());
        Assert.assertEquals(1, cell.getRealUsedSlots());
        Assert.assertEquals(16, cell.getAmountOf(diamond));

        final Inventory ui = player.getOpenInventory().getTopInventory();
        Assert.assertEquals(ui.getItem(0).getType(), Material.DIAMOND);
    }

    @Test
    public void testClickCentral() {
        player.mokkit().rightClickBlock(player.getLocation().getBlock(), BlockFace.UP);

        Assert.assertNotNull(player.getOpenInventory());
        Assert.assertTrue(SacknahtPlugin.getSelf().getInterfaceManager().isInInterface(player.getUniqueId()));

        // Click the central icon.
        TestHelper.assertThrows(() -> player.mokkit().clickInTopInventory(49, ClickType.LEFT, 0),
                CancelledByEventException.class);
    }

    @Test
    public void testTake() {
        player.mokkit().rightClickBlock(player.getLocation().getBlock(), BlockFace.UP);
        Assert.assertNotNull(player.getOpenInventory());
        Assert.assertTrue(SacknahtPlugin.getSelf().getInterfaceManager().isInInterface(player.getUniqueId()));

        final ItemPattern diamond = new ItemPattern(new ItemStack(Material.DIAMOND));
        player.getInventory().setItem(10, new ItemStack(Material.DIAMOND, 16));

        // Add the item.
        player.mokkit().clickInBottomInventory(10, ClickType.LEFT, 0);
        TestHelper.assertThrows(() -> player.mokkit().clickInTopInventory(0, ClickType.LEFT, 0),
                CancelledByEventException.class);

        final Inventory ui = player.getOpenInventory().getTopInventory();
        Assert.assertEquals(ui.getItem(0).getType(), Material.DIAMOND);
        Assert.assertEquals(16, cell.getAmountOf(diamond));

        // Take the item.
        TestHelper.assertThrows(() -> player.mokkit().clickInTopInventory(0, ClickType.LEFT, 0),
                CancelledByEventException.class);
        player.mokkit().clickInBottomInventory(5, ClickType.LEFT, 0);

        Assert.assertNull(ui.getItem(0));
        Assert.assertEquals(0, cell.getAmountOf(diamond));
    }
}
