package de.fuchspfoten.sacknaht.test.search;

import de.fuchspfoten.fuchslib.util.Pair;
import de.fuchspfoten.sacknaht.search.QGramIndex;
import de.fuchspfoten.sacknaht.test.SacknahtTest;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Test for {@link QGramIndex}es.
 */
public class QGramIndexTest extends SacknahtTest {

    @Test
    public void testSearch() {
        final QGramIndex<String> index = new QGramIndex<>(3);
        index.addDocument("abc", "abc");
        index.addDocument("adc", "adc");
        index.addDocument("def", "def");
        index.addDocument("ghi", "ghi");
        index.addDocument("ac", "ac");

        final List<Pair<String, Integer>> results = index.fuzzyFind(Collections.singletonList("abc"), 2);
        Assert.assertEquals(3, results.size());
        Assert.assertEquals("abc", results.get(0).getKey());
        Assert.assertEquals(0, results.get(0).getValue().intValue());
        Assert.assertEquals("adc", results.get(1).getKey());
        Assert.assertEquals(1, results.get(1).getValue().intValue());
        Assert.assertEquals("ac", results.get(2).getKey());
        Assert.assertEquals(1, results.get(2).getValue().intValue());
    }
}
