package de.fuchspfoten.sacknaht.test.search;

import de.fuchspfoten.fuchslib.util.Pair;
import de.fuchspfoten.sacknaht.search.MaterialIndex;
import de.fuchspfoten.sacknaht.test.SacknahtTest;
import org.bukkit.Material;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Test for {@link de.fuchspfoten.sacknaht.search.MaterialIndex}es.
 */
public class MaterialIndexTest extends SacknahtTest {

    @Test
    public void testSearch() {
        final MaterialIndex matIndex = new MaterialIndex();
        matIndex.loadAll();

        final List<Pair<Material, Integer>> results =
                matIndex.fuzzyFind(Collections.singletonList("erde"), 3);

        Assert.assertTrue(1 <= results.size());
        Assert.assertEquals(Material.DIRT, results.get(0).getKey());
    }

    @Test
    public void testSearchTypo() {
        final MaterialIndex matIndex = new MaterialIndex();
        matIndex.loadAll();

        final List<Pair<Material, Integer>> results =
                matIndex.fuzzyFind(Collections.singletonList("erdä"), 3);

        Assert.assertTrue(1 <= results.size());
        Assert.assertEquals(Material.DIRT, results.get(0).getKey());
    }

    @Test
    public void testInvalidQuery() {
        final MaterialIndex matIndex = new MaterialIndex();
        matIndex.loadAll();

        final List<Pair<Material, Integer>> results =
                matIndex.fuzzyFind(Collections.singletonList("+/§"), 3);

        Assert.assertEquals(0, results.size());
    }
}
