package de.fuchspfoten.sacknaht.test.search;

import de.fuchspfoten.fuchslib.util.Pair;
import de.fuchspfoten.sacknaht.search.EnchantmentIndex;
import de.fuchspfoten.sacknaht.test.SacknahtTest;
import org.bukkit.enchantments.Enchantment;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

/**
 * Test for {@link de.fuchspfoten.sacknaht.search.EnchantmentIndex}es.
 */
public class EnchantmentIndexTest extends SacknahtTest {

    @Test
    public void testSearch() {
        final EnchantmentIndex matIndex = new EnchantmentIndex();
        matIndex.loadAll();

        final List<Pair<Enchantment, Integer>> results =
                matIndex.fuzzyFind(Collections.singletonList("protection"), 10);

        Assert.assertTrue(1 <= results.size());
        Assert.assertEquals(Enchantment.PROTECTION_ENVIRONMENTAL, results.get(0).getKey());
    }
}
