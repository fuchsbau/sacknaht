package de.fuchspfoten.sacknaht.modules;

import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

/**
 * Listener for using the backpack-like {@link de.fuchspfoten.sacknaht.model.StorageCell}s.
 */
public class BackpackOpenModule implements Listener {

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getHand() == EquipmentSlot.HAND) {
            if (event.hasItem()
                    && (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR)) {
                final ItemStack item = event.getItem();
                final String id = StorageCell.getIdOfCell(item);
                if (id != null) {
                    final StorageCell sc = SacknahtPlugin.getSelf().getCellManager().getExistingCell(id);
                    if (sc != null && sc.isBackpack() && sc.hasPermission(event.getPlayer().getUniqueId())) {
                        SacknahtPlugin.getSelf().getInterfaceManager().open(Collections.singleton(sc),
                                event.getPlayer());
                    }
                }
            }
        }
    }
}
