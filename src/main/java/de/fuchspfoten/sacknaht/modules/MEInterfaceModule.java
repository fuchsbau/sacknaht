package de.fuchspfoten.sacknaht.modules;

import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.ui.InterfaceManager;
import de.fuchspfoten.sacknaht.ui.MEInterface;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * Listens to inventory events and forwards them to {@link MEInterface}s.
 */
public class MEInterfaceModule implements Listener {

    @SuppressWarnings("deprecation")  // setCursor is fine.
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.isCancelled()) {
            return;
        }

        final InterfaceManager mgr = SacknahtPlugin.getSelf().getInterfaceManager();
        if (!mgr.isInInterface(event.getWhoClicked().getUniqueId())) {
            return;
        }

        // Block disallowed actions in the interface.
        switch (event.getAction()) {
            case HOTBAR_MOVE_AND_READD:
            case HOTBAR_SWAP:
            case CLONE_STACK:
            case COLLECT_TO_CURSOR:
            case UNKNOWN:
                event.setCancelled(true);
                return;
            default:
                break;
        }

        // Dropping is allowed.
        if (event.getClickedInventory() == null) {
            return;
        }

        // Cancel by default.
        event.setCancelled(true);
        final boolean hasCursor = event.getCursor() != null && event.getCursor().getType() != Material.AIR;
        final boolean hasItem = event.getCurrentItem() != null && event.getCurrentItem().getType() != Material.AIR;
        if (event.getClickedInventory() == event.getView().getTopInventory()) {
            // ME click.
            final boolean clickToolbar = event.getSlot() >= 45;
            switch (event.getClick()) {
                case LEFT:
                    if (hasCursor) {
                        // Put cursor in.
                        if (mgr.addStack(event.getCursor(), event.getWhoClicked())) {
                            event.setCursor(null);
                        }
                    } else {
                        if (hasItem) {
                            if (clickToolbar) {
                                // Toolbar click.
                                mgr.toolbarClick(event.getSlot(), event.getWhoClicked());
                            } else {
                                // Take one stack.
                                final ItemStack retrieved =
                                        mgr.takeStack(event.getCurrentItem(), event.getWhoClicked());
                                event.setCursor(retrieved);
                            }
                        }
                    }
                    break;
                case SHIFT_LEFT:
                    if (hasItem) {
                        if (!clickToolbar) {
                            // Move one stack down.
                            final ItemStack retrieved =
                                    mgr.takeStack(event.getCurrentItem(), event.getWhoClicked());
                            final Map<Integer, ItemStack> remaining =
                                    event.getWhoClicked().getInventory().addItem(retrieved);
                            for (final ItemStack remainingStack : remaining.values()) {
                                final boolean addBack = mgr.addStack(remainingStack, event.getWhoClicked());
                                assert addBack : "could not add back after taking";
                            }
                        }
                    }
                    break;
                case RIGHT:
                    if (hasCursor) {
                        // Put half the cursor in.
                        final int sizeHalf = Math.max(1, event.getCursor().getAmount() / 2);
                        final int resultSize = event.getCursor().getAmount() - sizeHalf;

                        final ItemStack half = event.getCursor().clone();
                        half.setAmount(sizeHalf);
                        if (mgr.addStack(half, event.getWhoClicked())) {
                            if (resultSize == 0) {
                                event.setCursor(null);
                            } else {
                                half.setAmount(resultSize);
                                event.setCursor(half);
                            }
                        }
                    } else {
                        if (hasItem) {
                            if (!clickToolbar) {
                                // Take half a stack.
                                final ItemStack retrieved =
                                        mgr.takeHalfStack(event.getCurrentItem(), event.getWhoClicked());
                                event.setCursor(retrieved);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        } else {
            // Inventory click.
            switch (event.getClick()) {
                case SHIFT_LEFT:
                    // Move up.
                    if (hasItem) {
                        if (mgr.addStack(event.getCurrentItem(), event.getWhoClicked())) {
                            event.setCurrentItem(null);
                        }
                    }
                    break;
                case DROP:
                case CONTROL_DROP:
                case RIGHT:
                case LEFT:
                    event.setCancelled(false);
                    break;
                default:
                    break;
            }
        }
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        final InterfaceManager mgr = SacknahtPlugin.getSelf().getInterfaceManager();
        mgr.onClose(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (event.isCancelled()) {
            return;
        }

        final InterfaceManager mgr = SacknahtPlugin.getSelf().getInterfaceManager();
        if (!mgr.isInInterface(event.getWhoClicked().getUniqueId())) {
            return;
        }

        // A raw slot is in the top inventory if it is smaller than the size of the top inventory.
        for (final int raw : event.getRawSlots()) {
            if (raw < event.getView().getTopInventory().getSize()) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        final InterfaceManager mgr = SacknahtPlugin.getSelf().getInterfaceManager();
        mgr.onClose(event.getPlayer().getUniqueId());
    }
}
