package de.fuchspfoten.sacknaht.modules;

import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

/**
 * Listener for using the backpack-like {@link StorageCell}s.
 */
public class CellInitializerModule implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getHand() == EquipmentSlot.HAND) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                final ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
                final StorageCell newlyCreated = StorageCell.createFromUninitialized(item,
                        event.getPlayer().getUniqueId());
                if (newlyCreated != null) {
                    event.getPlayer().getInventory().setItemInMainHand(newlyCreated.getItem());
                }
            }
        }
    }
}
