package de.fuchspfoten.sacknaht.modules;

import de.fuchspfoten.itemtool.ItemStorage;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Wood;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

/**
 * A module which makes random special drops occur in some worlds.
 */
public class CraftingDropsModule implements Listener {

    /**
     * The random number generator for this module.
     */
    private final Random random = new Random();

    /**
     * Oak bark that can be obtained.
     */
    private final ItemStack barkOak;

    /**
     * Spruce bark that can be obtained.
     */
    private final ItemStack barkSpruce;

    /**
     * Gallnuts that can be obtained.
     */
    private final ItemStack gallnut;

    /**
     * The tainted locations for item retrieval (log breaking).
     */
    private final Collection<Location> taintedLocations = new HashSet<>();

    /**
     * Constructor.
     */
    public CraftingDropsModule() {
        final ItemStorage storage = Bukkit.getServicesManager().load(ItemStorage.class);
        barkOak = storage.load("ingredient.bark.oak");
        barkSpruce = storage.load("ingredient.bark.spruce");
        gallnut = storage.load("ingredient.gallnut");
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeavesDecay(final LeavesDecayEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!SacknahtPlugin.getSelf().canHaveDrops(event.getBlock().getWorld())) {
            return;
        }

        final Wood wood = (Wood) event.getBlock().getState().getData();
        switch (wood.getSpecies()) {
            case GENERIC:
            case DARK_OAK:
                break;
            default:
                return;
        }

        // Random chance of 1.5%.
        if (random.nextDouble() > 0.015) {
            return;
        }
        event.getBlock().getWorld().dropItem(event.getBlock().getLocation().add(0.5, 0.5, 0.5),
                gallnut.clone());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(final BlockPlaceEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (event.getBlockPlaced().getType() == Material.LOG || event.getBlockPlaced().getType() == Material.LOG_2) {
            taintedLocations.add(event.getBlockPlaced().getLocation());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockBreak(final BlockBreakEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!SacknahtPlugin.getSelf().canHaveDrops(event.getBlock().getWorld())) {
            return;
        }

        if (taintedLocations.contains(event.getBlock().getLocation())) {
            return;
        }

        final ItemStack inHand = event.getPlayer().getInventory().getItemInMainHand();
        if (inHand != null) {
            if (inHand.getEnchantmentLevel(Enchantment.SILK_TOUCH) != 0) {
                return;
            }
            if (inHand.getType() == Material.SHEARS) {
                return;
            }
        }

        final Block block = event.getBlock();
        final boolean leaves;
        switch (block.getType()) {
            case LOG:
            case LOG_2:
                leaves = false;
                break;
            case LEAVES:
            case LEAVES_2:
                leaves = true;
                break;
            default:
                return;
        }

        // Get the type of log/leaves.
        final Wood wood = (Wood) block.getState().getData();
        switch (wood.getSpecies()) {
            case GENERIC:
            case DARK_OAK:
            case REDWOOD:
                break;
            default:
                return;
        }

        // Random chance of 3%.
        if (random.nextDouble() > 0.03) {
            return;
        }

        if (leaves && wood.getSpecies() != TreeSpecies.REDWOOD) {
            // Gall nut drop chance is 1.5% -> 50% at this point.
            if (random.nextDouble() > 0.5) {
                return;
            }
            block.getWorld().dropItem(block.getLocation().add(0.5, 0.5, 0.5), gallnut.clone());
        } else if (!leaves) {
            final ItemStack drop;
            if (wood.getSpecies() == TreeSpecies.REDWOOD) {
                drop = barkSpruce.clone();
            } else {
                drop = barkOak.clone();
            }
            block.getWorld().dropItem(block.getLocation().add(0.5, 0.5, 0.5), drop);
        }
    }
}
