package de.fuchspfoten.sacknaht.modules;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.CellType;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

/**
 * /me manages storage cells.
 */
public class MECommandModule implements CommandExecutor {

    /**
     * The parser for command arguments for /me.
     */
    private final ArgumentParser argumentParserME;

    /**
     * Constructor.
     */
    public MECommandModule() {
        argumentParserME = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("sacknaht.admin")) {
            sender.sendMessage("Missing permission: sacknaht.admin");
            return true;
        }

        // Syntax check.
        args = argumentParserME.parse(args);
        if (argumentParserME.hasArgument('h') || !(sender instanceof Player)) {
            Messenger.send(sender, "commandHelp", "Syntax: /me <create <w> <h> [b]" +
                    "|modify <w> <h> [b]" +
                    "|get <id>" +
                    "|open>");
            argumentParserME.showHelp(sender);
            return true;
        }
        final Player player = (Player) sender;

        if (args.length >= 3 && args[0].equals("create")) {
            // Get the limits.
            final int limitSlots;
            final int limitSlotSize;
            try {
                limitSlots = Integer.parseInt(args[1]);
                limitSlotSize = Integer.parseInt(args[2]);
                if (limitSlots <= 0 || limitSlotSize <= 0) {
                    return true;
                }
            } catch (final NumberFormatException ex) {
                return true;
            }

            final CellType type = args.length == 4 ? CellType.BACKPACK : CellType.READER_ONLY;
            final ItemStack item = StorageCell.getUninitialized(limitSlots, limitSlotSize, type);
            player.getInventory().addItem(item);
            sender.sendMessage("obtained item");
        } else if (args.length == 1 && args[0].equals("open")) {
            final String id = StorageCell.getIdOfCell(player.getInventory().getItemInMainHand());
            if (id != null) {
                final StorageCell sc = SacknahtPlugin.getSelf().getCellManager().getExistingCell(id);
                if (sc != null) {
                    SacknahtPlugin.getSelf().getInterfaceManager().open(Collections.singleton(sc), player);
                }
            }
        } else if (args.length == 2 && args[0].equals("get")) {
            final String id = args[1];
            final StorageCell sc = SacknahtPlugin.getSelf().getCellManager().getExistingCell(id);
            if (sc != null) {
                player.getInventory().addItem(sc.getItem());
                player.updateInventory();
            }
        } else if (args.length >= 3 && args[0].equals("modify")) {
            // Get the limits.
            final int limitSlots;
            final int limitSlotSize;
            try {
                limitSlots = Integer.parseInt(args[1]);
                limitSlotSize = Integer.parseInt(args[2]);
                if (limitSlots <= 0 || limitSlotSize <= 0) {
                    return true;
                }
            } catch (final NumberFormatException ex) {
                return true;
            }

            final String id = StorageCell.getIdOfCell(player.getInventory().getItemInMainHand());
            if (id != null) {
                final StorageCell sc = SacknahtPlugin.getSelf().getCellManager().getExistingCell(id);
                sc.setMaxSlots(limitSlots);
                sc.setMaxItemsPerSlot(limitSlotSize);
                sc.setBackpack(args.length == 4);
                sender.sendMessage("done");
            }
        }

        return true;
    }
}
