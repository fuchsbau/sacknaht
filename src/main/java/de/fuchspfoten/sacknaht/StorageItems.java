package de.fuchspfoten.sacknaht;

import de.fuchspfoten.fuchslib.item.CustomItems;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import org.bukkit.inventory.ItemStack;

/**
 * Different storage cell item patterns.
 */
public final class StorageItems {

    /**
     * An item matcher for uninitialized backpacks.
     */
    public static final ItemMatcher UNINITIALIZED_BACKPACK;

    /**
     * An item matcher for uninitialized cell.
     */
    public static final ItemMatcher UNINITIALIZED_CELL;

    /**
     * An item matcher for backpacks.
     */
    public static final ItemMatcher BACKPACK;

    /**
     * An item matcher for cells.
     */
    public static final ItemMatcher CELL;

    static {
        final ItemStack uninitializedBackpack = CustomItems.BACKPACK.getFactory()
                .name("§3Rucksack")
                .lore("§eOriginalverpackt", "§8{0:d}/{1:d}")
                .instance();
        UNINITIALIZED_BACKPACK = new ItemMatcher(uninitializedBackpack);

        final ItemStack uninitializedCell = CustomItems.BACKPACK_GOLDEN.getFactory()
                .name("§3Multi-Pack")
                .lore("§eOriginalverpackt", "§8{0:d}/{1:d}")
                .instance();
        UNINITIALIZED_CELL = new ItemMatcher(uninitializedCell);

        final ItemStack backpack = CustomItems.BACKPACK.getFactory()
                .name("§3Rucksack")
                .lore("§8{0:s}")
                .instance();
        BACKPACK = new ItemMatcher(backpack);

        final ItemStack cell = CustomItems.BACKPACK_GOLDEN.getFactory()
                .name("§3Multi-Pack")
                .lore("§8{0:s}")
                .instance();
        CELL = new ItemMatcher(cell);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private StorageItems() {
    }
}
