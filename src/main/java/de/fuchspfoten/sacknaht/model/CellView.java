package de.fuchspfoten.sacknaht.model;

import de.fuchspfoten.sacknaht.data.AVLContentStorage;
import de.fuchspfoten.sacknaht.data.ContentIterator;
import de.fuchspfoten.sacknaht.data.ContentStorage;
import de.fuchspfoten.sacknaht.model.ActionOutcome.ActionResult;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * A view of possibly many cells to delegate actions to its contents, acting as one.
 */
public class CellView implements ICell {

    /**
     * The filtering predicate.
     */
    private final Predicate<ItemPattern> filter;

    /**
     * The cells to which this view delegates.
     */
    private final Collection<StorageCell> delegates = new ArrayList<>();

    /**
     * A cached view of the items in the delegates. This should only ever be used for display, never for logic!
     */
    private final ContentStorage contentCache = new AVLContentStorage();

    /**
     * Constructor.
     *
     * @param cells The cells for this view.
     * @param filter The filtering predicate for the view.
     */
    public CellView(final Collection<StorageCell> cells, final Predicate<ItemPattern> filter) {
        this.filter = filter;
        delegates.addAll(cells);

        // Create the cache.
        for (final StorageCell cell : cells) {
            cell.mergeInto(contentCache, filter);
        }
    }

    @Override
    public ActionOutcome addItem(final ItemPattern pattern, final long amount) {
        // Try adding to existing storage.
        for (final StorageCell cell : delegates) {
            if (cell.containsType(pattern)) {
                final ActionOutcome result = cell.addItem(pattern, amount);
                if (result.getResult() == ActionResult.OK) {
                    add(pattern, amount);
                }
                return result;
            }
        }

        // Try adding to cells with free space.
        for (final StorageCell cell : delegates) {
            if (cell.getRealUsedSlots() < cell.getMaxSlots()) {
                final ActionOutcome result = cell.addItem(pattern, amount);
                if (result.getResult() == ActionResult.OK) {
                    add(pattern, amount);
                }
                return result;
            }
        }

        // Nowhere to add this item.
        return new ActionOutcome(ActionResult.OUT_OF_SPACE, null);
    }

    @Override
    public long getAmountOf(final ItemPattern pattern) {
        long amount = 0;
        for (final StorageCell backend : delegates) {
            amount += backend.getAmountOf(pattern);
        }
        return amount;
    }

    /**
     * Returns a part of the content, formatted for output.
     *
     * @param amount The number of entries to get.
     * @param skip   The number of entries to skip.
     * @return The entries that are in this part.
     */
    public List<ItemStack> getContentPart(final int amount, final int skip) {
        final List<ItemStack> result = new ArrayList<>();

        if (contentCache.size() <= skip) {
            return result;
        }

        ContentIterator cursor = contentCache.get(skip);
        int left = amount;
        while (cursor != null) {
            if (left-- == 0) {
                break;
            }
            result.add(cursor.getPattern().escapeItem(cursor.getAmount()));
            cursor = cursor.next();
        }
        return result;
    }

    @Override
    public int getMaxItemsPerSlot() {
        int num = 0;
        for (final StorageCell cell : delegates) {
            num += cell.getMaxItemsPerSlot();
        }
        return num;
    }

    @Override
    public int getMaxSlots() {
        int num = 0;
        for (final StorageCell cell : delegates) {
            num += cell.getMaxSlots();
        }
        return num;
    }

    @Override
    public int getRealUsedSlots() {
        int num = 0;
        for (final StorageCell cell : delegates) {
            num += cell.getRealUsedSlots();
        }
        return num;
    }

    @Override
    public int getUsedSlots() {
        return contentCache.size();
    }

    @Override
    public long take(final ItemPattern pattern, final long amount) {
        final long have = getAmountOf(pattern);
        long toTake = have >= amount ? amount : have;
        final long maximumTake = toTake;

        // Update the cache.
        final long newAmount = have - toTake;
        if (newAmount == 0) {
            contentCache.remove(pattern);
        } else {
            contentCache.put(pattern, newAmount);
        }

        for (final StorageCell cell : delegates) {
            toTake -= cell.take(pattern, toTake);
            if (toTake == 0) {
                return maximumTake;
            }
        }
        return maximumTake - toTake;
    }

    /**
     * Adds the given stack to the content cache.
     *
     * @param pattern The pattern.
     * @param amount  The amount.
     */
    private void add(final ItemPattern pattern, final long amount) {
        if (filter == null || filter.test(pattern)) {
            contentCache.add(pattern, amount);
        }
    }
}
