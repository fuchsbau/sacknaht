package de.fuchspfoten.sacknaht.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.bukkit.inventory.ItemStack;

/**
 * An outcome of an action.
 */
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class ActionOutcome {

    /**
     * The result of the action.
     */
    private final ActionResult result;

    /**
     * The item stack that is the outcome of the action.
     */
    private final ItemStack outcome;

    /**
     * The possible results of an action in the interface.
     */
    public enum ActionResult {

        /**
         * Indicates a successful action.
         */
        OK,

        /**
         * Indicates a failure due to insufficient space.
         */
        OUT_OF_SPACE,

        /**
         * Indicates a failure due to a banned item.
         */
        BANNED,

        /**
         * Indicates that an invalid item was requested.
         */
        NOT_IN_SYNC,

        /**
         * Indicates that no action occurred.
         */
        NOOP
    }
}
