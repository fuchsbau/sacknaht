package de.fuchspfoten.sacknaht.model;

import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.StorageItems;
import de.fuchspfoten.sacknaht.data.AVLContentStorage;
import de.fuchspfoten.sacknaht.data.ContentIterator;
import de.fuchspfoten.sacknaht.data.ContentStorage;
import de.fuchspfoten.sacknaht.model.ActionOutcome.ActionResult;
import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A storage cell.
 */
public final class StorageCell implements ICell {

    /**
     * Creates a storage cell.
     *
     * @param id            The ID of the cell.
     * @param owner         The owner of the cell.
     * @param limitSlots    The maximal number of slots this cell may contain.
     * @param limitSlotSize The maximal number of items a single slot may contain.
     * @param type          The type of storage cell.
     * @return The cell.
     */
    public static StorageCell createCell(final String id, final UUID owner, final int limitSlots,
                                         final int limitSlotSize, final CellType type) {
        final StorageCell cell = new StorageCell(SacknahtPlugin.getSelf().getCellDataFile(id), id, owner, limitSlots,
                limitSlotSize, type);
        cell.dirty = true;
        cell.saveCell();
        return cell;
    }

    /**
     * Attempts to create a storage cell from an uninitialized storage cell.
     *
     * @param item  The uninitialized storage cell.
     * @param owner The new owner.
     * @return The created cell, or null.
     */
    public static StorageCell createFromUninitialized(final ItemStack item, final UUID owner) {
        final ItemMatcher cellMatcher = StorageItems.UNINITIALIZED_CELL;
        cellMatcher.reset(item);
        if (cellMatcher.isMatch()) {
            return SacknahtPlugin.getSelf().getCellManager().getNewCell(owner, cellMatcher.getInt(0),
                    cellMatcher.getInt(1), CellType.READER_ONLY);
        }

        final ItemMatcher backpackMatcher = StorageItems.UNINITIALIZED_BACKPACK;
        backpackMatcher.reset(item);
        if (backpackMatcher.isMatch()) {
            return SacknahtPlugin.getSelf().getCellManager().getNewCell(owner, backpackMatcher.getInt(0),
                    backpackMatcher.getInt(1), CellType.BACKPACK);
        }

        return null;
    }

    /**
     * Loads the cell from the given data file.
     *
     * @param dataFile The data file.
     * @return The cell.
     */
    public static StorageCell loadCell(final DataFile dataFile) {
        final StorageCell cell = new StorageCell(dataFile, dataFile.getStorage().getString("id"),
                UUID.fromString(dataFile.getStorage().getString("owner")),
                dataFile.getStorage().getInt("limitSlots"),
                dataFile.getStorage().getInt("limitSlotSize"),
                dataFile.getStorage().getBoolean("backpack") ? CellType.BACKPACK : CellType.READER_ONLY);
        dataFile.getStorage().getStringList("members").stream().map(UUID::fromString).forEach(cell.members::add);

        final ConfigurationSection contentSection = dataFile.getStorage().getConfigurationSection("contents");
        for (final String key : contentSection.getKeys(false)) {
            final ConfigurationSection itemSection = contentSection.getConfigurationSection(key);
            final ItemSerializer item = ItemSerializer.load(itemSection);
            cell.contents.put(item.getPattern(), item.getAmount());
        }
        return cell;
    }

    /**
     * Fetches the ID of a potential storage cell, null if invalid.
     *
     * @param item The storage cell.
     * @return The ID.
     */
    public static String getIdOfCell(final ItemStack item) {
        StorageItems.BACKPACK.reset(item);
        if (StorageItems.BACKPACK.isMatch()) {
            return StorageItems.BACKPACK.getString(0);
        }

        StorageItems.CELL.reset(item);
        if (StorageItems.CELL.isMatch()) {
            return StorageItems.CELL.getString(0);
        }

        return null;
    }

    /**
     * Retrieves an uninitialized item of a storage cell.
     *
     * @param limitSlots    The slot limit.
     * @param limitSlotSize The slot size limit.
     * @param type          The type of the storage cell.
     * @return The item.
     */
    public static ItemStack getUninitialized(final long limitSlots, final long limitSlotSize, final CellType type) {
        switch (type) {
            case BACKPACK:
                return StorageItems.UNINITIALIZED_BACKPACK.interpolate(limitSlots, limitSlotSize);
            case READER_ONLY:
                return StorageItems.UNINITIALIZED_CELL.interpolate(limitSlots, limitSlotSize);
        }
        throw new IllegalStateException("unknown type, not handled");
    }

    /**
     * The data file of this storage cell.
     */
    private final DataFile dataFile;

    /**
     * The items stored in the cell.
     */
    private final ContentStorage contents = new AVLContentStorage();

    /**
     * The ID of the cell.
     */
    private final String cellId;

    /**
     * The owner of the cell.
     */
    private final UUID owner;

    /**
     * The members of this cell.
     */
    private @Getter final Set<UUID> members = new HashSet<>();

    /**
     * The slot limit.
     */
    private @Getter int maxSlots;

    /**
     * The slot size limit.
     */
    private @Getter int maxItemsPerSlot;

    /**
     * Whether this cell is backpack-like and can be opened without a terminal.
     */
    private @Getter boolean backpack;

    /**
     * Whether this cell needs to be saved.
     */
    private boolean dirty;

    /**
     * Constructor.
     *
     * @param dataFile        The data file of the cell.
     * @param cellId          The ID of the cell.
     * @param owner           The owner of the cell.
     * @param maxSlots        The maximal number of slots this cell may contain.
     * @param maxItemsPerSlot The maximal number of items a single slot may contain.
     * @param type            The type of cell to create.
     */
    private StorageCell(final DataFile dataFile, final String cellId, final UUID owner, final int maxSlots,
                        final int maxItemsPerSlot, final CellType type) {
        this.dataFile = dataFile;
        this.cellId = cellId;
        this.owner = owner;
        this.maxSlots = maxSlots;
        this.maxItemsPerSlot = maxItemsPerSlot;
        backpack = type == CellType.BACKPACK;
        dirty = false;
    }

    /**
     * Checks whether or not this cell contains items of the given pattern.
     *
     * @param pattern The pattern.
     * @return Whether this cell contains items matching the pattern.
     */
    public boolean containsType(final ItemPattern pattern) {
        return contents.get(pattern) != null;
    }

    /**
     * Gets the item for this storage cell.
     *
     * @return The item.
     */
    public ItemStack getItem() {
        return (backpack ? StorageItems.BACKPACK : StorageItems.CELL).interpolate(cellId);
    }

    /**
     * Checks whether or not the given UUID has permission to use this cell.
     *
     * @param who The UUID.
     * @return Whether the UUID has permission or not.
     */
    public boolean hasPermission(final UUID who) {
        return owner.equals(who) || members.contains(who);
    }

    /**
     * Checks whether or not the given user can manage this storage cell.
     *
     * @param who The user.
     * @return True iff the user can manage this cell.
     */
    public boolean canManage(final Entity who) {
        return owner.equals(who.getUniqueId()) || who.hasPermission("sacknaht.admin");
    }

    /**
     * Merges the contents of this cell into the given target storage.
     *
     * @param target The target storage.
     * @param filter The filtering predicate.
     */
    public void mergeInto(final ContentStorage target, final Predicate<ItemPattern> filter) {
        if (filter == null) {
            target.merge(contents);
        } else {
            if (contents.size() == 0) {
                return;
            }

            ContentIterator cursor = contents.get(0);
            while (cursor != null) {
                if (filter.test(cursor.getPattern())) {
                    target.add(cursor.getPattern(), cursor.getAmount());
                }
                cursor = cursor.next();
            }
        }
    }

    /**
     * Saves the cell.
     */
    public void saveCell() {
        if (!dirty) {
            return;
        }
        dirty = false;

        dataFile.getStorage().set("owner", owner.toString());
        dataFile.getStorage().set("id", cellId);
        dataFile.getStorage().set("limitSlots", maxSlots);
        dataFile.getStorage().set("limitSlotSize", maxItemsPerSlot);
        dataFile.getStorage().set("backpack", backpack);
        dataFile.getStorage().set("members", members.stream().map(UUID::toString).collect(Collectors.toList()));

        // Replace contents.
        final ConfigurationSection contentSection = dataFile.getStorage().createSection("contents");
        if (contents.size() > 0) {
            ContentIterator cursor = contents.get(0);
            int id = 0;
            while (cursor != null) {
                final ConfigurationSection itemSection =
                        contentSection.createSection(Integer.toString(id, Character.MAX_RADIX));
                final ItemSerializer itemStored = new ItemSerializer(cursor.getPattern(), cursor.getAmount());
                itemStored.save(itemSection);
                id++;
                cursor = cursor.next();
            }
        }
        dataFile.save(StorageCellVersionUpdater.VERSION);
    }

    /**
     * Sets whether or not this cell is a backpack.
     *
     * @param backpack Whether this cell is a backpack.
     */
    public void setBackpack(final boolean backpack) {
        dirty = true;
        this.backpack = backpack;
    }

    /**
     * Sets the maximum number of items per slot.
     *
     * @param maxItemsPerSlot The maximum number of items per slot.
     */
    public void setMaxItemsPerSlot(final int maxItemsPerSlot) {
        dirty = true;
        this.maxItemsPerSlot = maxItemsPerSlot;
    }

    /**
     * Sets the maximum number of slots.
     *
     * @param maxSlots The maximum number of slots.
     */
    public void setMaxSlots(final int maxSlots) {
        dirty = true;
        this.maxSlots = maxSlots;
    }

    /**
     * Marks the cell as dirty if the contents were modified in any
     */
    public void makeDirty() {
        dirty = true;
    }

    @Override
    public ActionOutcome addItem(final ItemPattern pattern, final long amount) {
        if (pattern.isBanned()) {
            return new ActionOutcome(ActionResult.BANNED, null);
        }

        final ContentIterator old = contents.get(pattern);
        if (old == null) {
            // Add as a new item.
            if (contents.size() >= maxSlots || amount > maxItemsPerSlot) {
                return new ActionOutcome(ActionResult.OUT_OF_SPACE, null);
            }
            contents.put(pattern, amount);
        } else {
            // Add as an existing item.
            if (old.getAmount() + amount > maxItemsPerSlot) {
                return new ActionOutcome(ActionResult.OUT_OF_SPACE, null);
            }
            contents.add(pattern, amount);
        }
        dirty = true;
        return new ActionOutcome(ActionResult.OK, null);
    }

    @Override
    public long getAmountOf(final ItemPattern pattern) {
        final ContentIterator entry = contents.get(pattern);
        return entry == null ? 0 : entry.getAmount();
    }

    @Override
    public int getRealUsedSlots() {
        return getUsedSlots();
    }

    @Override
    public int getUsedSlots() {
        return contents.size();
    }

    @Override
    public long take(final ItemPattern pattern, final long amount) {
        final long have = getAmountOf(pattern);
        dirty = true;
        if (have >= amount) {
            final long newHave = have - amount;
            if (newHave == 0) {
                contents.remove(pattern);
            } else {
                contents.put(pattern, newHave);
            }
            return amount;
        }
        contents.remove(pattern);
        return have;
    }

    /**
     * An item which is used for serializing big item stacks.
     */
    private static class ItemSerializer {

        /**
         * Loads an item.
         *
         * @param itemSection The configuration section.
         * @return The loaded item.
         */
        public static ItemSerializer load(final ConfigurationSection itemSection) {
            return new ItemSerializer(new ItemPattern(itemSection.getItemStack("i")),
                    itemSection.getLong("n"));
        }

        /**
         * The pattern item.
         */
        private @Getter final ItemPattern pattern;

        /**
         * How much of the pattern this ItemSerializer contains.
         */
        private @Getter final long amount;

        /**
         * Constructor.
         *
         * @param pattern The item pattern.
         * @param amount  The amount this ItemSerializer contains of the pattern.
         */
        public ItemSerializer(final ItemPattern pattern, final long amount) {
            this.pattern = pattern;
            this.amount = amount;
        }

        /**
         * Saves this item to the given section.
         *
         * @param itemSection The section.
         */
        public void save(final ConfigurationSection itemSection) {
            itemSection.set("n", amount);
            itemSection.set("i", pattern.getSample());
        }
    }
}
