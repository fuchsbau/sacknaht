package de.fuchspfoten.sacknaht.model;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Manages the member management conversation.
 */
public class MemberManager {

    /**
     * The conversation factory.
     */
    private final ConversationFactory conversationFactory;

    /**
     * Constructor.
     */
    public MemberManager(final JavaPlugin owner) {
        conversationFactory = new ConversationFactory(owner)
                .withModality(false)
                .withLocalEcho(false)
                .withEscapeSequence("quit")
                .addConversationAbandonedListener(
                        c -> Messenger.send((CommandSender) c.getContext().getForWhom(), "sacknaht.quitManage"));
    }

    /**
     * Opens a member management prompt for the given storage cell.
     *
     * @param target     The player.
     * @param targetCell The target cell.
     */
    public void openPrompt(final Conversable target, final StorageCell targetCell) {
        conversationFactory.withFirstPrompt(new ShowAllPrompt(targetCell)).buildConversation(target).begin();
    }

    /**
     * Shows all members of the cells.
     */
    @RequiredArgsConstructor
    private class ShowAllPrompt extends MessagePrompt {

        /**
         * The target cell.
         */
        private final StorageCell targetCell;

        @Override
        public String getPromptText(final ConversationContext context) {
            return String.format(Messenger.getFormat("sacknaht.showall"), String.join(", ",
                    targetCell.getMembers().stream()
                            .map(Bukkit::getOfflinePlayer)
                            .map(OfflinePlayer::getName)
                            .collect(Collectors.toList())));
        }

        @Override
        protected Prompt getNextPrompt(final ConversationContext context) {
            return new ManagePrompt(targetCell);
        }

        @Override
        public ShowAllPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }

    /**
     * Accepts one management input.
     */
    @RequiredArgsConstructor
    private class ManagePrompt extends StringPrompt {

        /**
         * The target cell.
         */
        private final StorageCell targetCell;

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            if (!input.isEmpty()) {
                if (input.charAt(0) == '+' || input.charAt(0) == '-') {
                    final String name = input.substring(1);
                    if (!name.isEmpty()) {
                        final UUID uuid = UUIDLookup.lookupNonBlocking(name);
                        if (uuid != null) {
                            if (input.charAt(0) == '+') {
                                targetCell.getMembers().add(uuid);
                            } else {
                                targetCell.getMembers().remove(uuid);
                            }
                            targetCell.makeDirty();
                        } else {
                            Messenger.send((CommandSender) context.getForWhom(), "sacknaht.userNotExists");
                        }
                    }
                }
            }

            return new ShowAllPrompt(targetCell);
        }

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("sacknaht.manage");
        }

        @Override
        public ManagePrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
