package de.fuchspfoten.sacknaht.model;

/**
 * Represents something that can be interacted with like a cell.
 */
public interface ICell {

    /**
     * Adds the given item stack to the storage of this cell.
     *
     * @param pattern The stack.
     * @param amount  The amount to add.
     * @return The result of the action.
     */
    ActionOutcome addItem(final ItemPattern pattern, final long amount);

    /**
     * Gets the amount present of the given pattern.
     *
     * @param pattern The pattern.
     * @return The amount.
     */
    long getAmountOf(final ItemPattern pattern);

    /**
     * Retrieves the maximum number of items this cell-like object can have in a slot.
     *
     * @return The maximum number of items this cell-like object can have in a slot
     */
    int getMaxItemsPerSlot();

    /**
     * Retrieves the maximum number of slots this cell-like object can hold.
     *
     * @return The maximum number of slots.
     */
    int getMaxSlots();

    /**
     * Gets the real number of used slots without merging.
     *
     * @return The number of used slots without merging.
     */
    int getRealUsedSlots();

    /**
     * Retrieves the number of used slots in storage.
     *
     * @return The number of used slots in storage.
     */
    int getUsedSlots();

    /**
     * Takes the given number of items matching the pattern from the storage.
     *
     * @param pattern The pattern.
     * @param amount  The amount to take.
     * @return The actually taken amount.
     */
    long take(final ItemPattern pattern, final long amount);
}
