package de.fuchspfoten.sacknaht.model;

/**
 * The different types of storage cells.
 */
public enum CellType {

    /**
     * A cell readable as a backpack.
     */
    BACKPACK,

    /**
     * A cell only readable in reader machines.
     */
    READER_ONLY
}
