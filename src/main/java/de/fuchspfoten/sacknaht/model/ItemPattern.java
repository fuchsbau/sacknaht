package de.fuchspfoten.sacknaht.model;

import de.fuchspfoten.fuchslib.item.ItemFactory;
import de.fuchspfoten.sacknaht.StorageItems;
import de.fuchspfoten.sacknaht.Unsafe;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.banner.Pattern;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.KnowledgeBookMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.Repairable;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.SpawnEggMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;

import java.util.List;
import java.util.Map;

/**
 * A pattern of an item.
 */
public class ItemPattern implements Comparable<ItemPattern> {

    /**
     * Unescapes the given item.
     *
     * @param item The item.
     * @return The unescaped item.
     */
    public static ItemPattern unescapeItem(final ItemStack item) {
        final ItemFactory fac = new ItemFactory(item.clone()).unban().popLore(2);
        return new ItemPattern(fac.instance());
    }

    /**
     * Compares this pattern's banner meta with the given skull meta.
     *
     * @param oneBanner   The first meta.
     * @param otherBanner The other meta.
     * @return The comparison result.
     */
    private static int compareBanner(final BannerMeta oneBanner, final BannerMeta otherBanner) {
        if (oneBanner.numberOfPatterns() != otherBanner.numberOfPatterns()) {
            return Integer.compare(oneBanner.numberOfPatterns(), otherBanner.numberOfPatterns());
        }
        for (int i = 0; i < oneBanner.numberOfPatterns(); i++) {
            final Pattern onePattern = oneBanner.getPattern(i);
            final Pattern otherPattern = otherBanner.getPattern(i);
            if (onePattern.getPattern() != otherPattern.getPattern()) {
                return onePattern.getPattern().compareTo(otherPattern.getPattern());
            }
            if (onePattern.getColor() != otherPattern.getColor()) {
                return onePattern.getColor().compareTo(otherPattern.getColor());
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's book meta with the given skull meta.
     *
     * @param oneBook   The first meta.
     * @param otherBook The other meta.
     * @return The comparison result.
     */
    private static int compareBook(final BookMeta oneBook, final BookMeta otherBook) {
        if (oneBook.hasTitle() != otherBook.hasTitle()) {
            return oneBook.hasTitle() ? 1 : -1;
        }
        if (oneBook.hasTitle()) {
            final int cmpTitle = oneBook.getTitle().compareTo(otherBook.getTitle());
            if (cmpTitle != 0) {
                return cmpTitle;
            }
        }
        if (oneBook.hasAuthor() != otherBook.hasAuthor()) {
            return oneBook.hasAuthor() ? 1 : -1;
        }
        if (oneBook.hasAuthor()) {
            final int cmpAuthor = oneBook.getAuthor().compareTo(otherBook.getAuthor());
            if (cmpAuthor != 0) {
                return cmpAuthor;
            }
        }
        if (oneBook.hasGeneration() != otherBook.hasGeneration()) {
            return oneBook.hasGeneration() ? 1 : -1;
        }
        if (oneBook.hasGeneration()) {
            final int cmpGen = oneBook.getGeneration().compareTo(otherBook.getGeneration());
            if (cmpGen != 0) {
                return cmpGen;
            }
        }
        if (oneBook.getPageCount() != otherBook.getPageCount()) {
            return Integer.compare(oneBook.getPageCount(), otherBook.getPageCount());
        }
        for (int i = 1; i <= oneBook.getPageCount(); i++) {
            final int cmpPage = oneBook.getPage(i).compareTo(otherBook.getPage(i));
            if (cmpPage != 0) {
                return cmpPage;
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's firework effect meta with the given skull meta.
     *
     * @param oneEffect   The first meta.
     * @param otherEffect The other meta.
     * @return The comparison result.
     */
    private static int compareFireworkEffects(final FireworkEffectMeta oneEffect,
                                              final FireworkEffectMeta otherEffect) {
        if (oneEffect.hasEffect() != otherEffect.hasEffect()) {
            return oneEffect.hasEffect() ? 1 : -1;
        }
        if (oneEffect.hasEffect()) {
            final int cmpEffect = compareFireworkEffect(oneEffect.getEffect(), otherEffect.getEffect());
            if (cmpEffect != 0) {
                return cmpEffect;
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's firework meta with the given skull meta.
     *
     * @param oneFirework   The first meta.
     * @param otherFirework The other meta.
     * @return The comparison result.
     */
    private static int compareFireworks(final FireworkMeta oneFirework, final FireworkMeta otherFirework) {
        final int cmpPower = Integer.compare(oneFirework.getPower(), otherFirework.getPower());
        if (cmpPower != 0) {
            return cmpPower;
        }
        final List<FireworkEffect> oneEffects = oneFirework.getEffects();
        final List<FireworkEffect> otherEffects = otherFirework.getEffects();
        if (oneEffects.size() != otherEffects.size()) {
            return Integer.compare(oneEffects.size(), otherEffects.size());
        }
        final int cmpEffects = compareFireworkEffects(oneEffects, otherEffects);
        if (cmpEffects != 0) {
            return cmpEffects;
        }
        return 0;
    }

    /**
     * Compares this pattern's knowledgebook meta with the given skull meta.
     *
     * @param oneBook   The first meta.
     * @param otherBook The other meta.
     * @return The comparison result.
     */
    private static int compareKnowledgeBooks(final KnowledgeBookMeta oneBook, final KnowledgeBookMeta otherBook) {
        if (oneBook.getRecipes().size() != otherBook.getRecipes().size()) {
            return Integer.compare(oneBook.getRecipes().size(), otherBook.getRecipes().size());
        }
        final List<NamespacedKey> oneList = oneBook.getRecipes();
        final List<NamespacedKey> otherList = otherBook.getRecipes();
        for (int recipe = 0; recipe < oneList.size(); recipe++) {
            final NamespacedKey oneKey = oneList.get(recipe);
            final NamespacedKey otherKey = otherList.get(recipe);
            final int cmpNamespace = oneKey.getNamespace().compareTo(otherKey.getNamespace());
            if (cmpNamespace != 0) {
                return cmpNamespace;
            }
            final int cmpKey = oneKey.getKey().compareTo(otherKey.getKey());
            if (cmpKey != 0) {
                return cmpKey;
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's map meta with the given skull meta.
     *
     * @param oneMap   The first meta.
     * @param otherMap The other meta.
     * @return The comparison result.
     */
    private static int compareMaps(final MapMeta oneMap, final MapMeta otherMap) {
        if (oneMap.isScaling() != otherMap.isScaling()) {
            return oneMap.isScaling() ? 1 : -1;
        }
        if (oneMap.hasLocationName() != otherMap.hasLocationName()) {
            return oneMap.hasLocationName() ? 1 : -1;
        }
        if (oneMap.hasLocationName()) {
            final int cmp = oneMap.getLocationName().compareTo(otherMap.getLocationName());
            if (cmp != 0) {
                return cmp;
            }
        }
        if (oneMap.hasColor() != otherMap.hasColor()) {
            return oneMap.hasColor() ? 1 : -1;
        }
        if (oneMap.hasColor()) {
            final int cmp = Integer.compare(oneMap.getColor().asRGB(), otherMap.getColor().asRGB());
            if (cmp != 0) {
                return cmp;
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's potion meta with the given skull meta.
     *
     * @param onePotion   The first meta.
     * @param otherPotion The other meta.
     * @return The comparison result.
     */
    private static int comparePotions(final PotionMeta onePotion, final PotionMeta otherPotion) {
        final int cmpData = comparePotionData(onePotion.getBasePotionData(), otherPotion.getBasePotionData());
        if (cmpData != 0) {
            return cmpData;
        }
        if (onePotion.hasCustomEffects() != otherPotion.hasCustomEffects()) {
            return onePotion.hasCustomEffects() ? 1 : -1;
        }
        if (onePotion.hasCustomEffects()) {
            final int cmpEffects = compareCustomEffects(onePotion.getCustomEffects(),
                    otherPotion.getCustomEffects());
            if (cmpEffects != 0) {
                return cmpEffects;
            }
        }
        if (onePotion.hasColor() != otherPotion.hasColor()) {
            return onePotion.hasColor() ? 1 : -1;
        }
        if (onePotion.hasColor()) {
            final int cmpColor = Integer.compare(onePotion.getColor().asRGB(), otherPotion.getColor().asRGB());
            if (cmpColor != 0) {
                return cmpColor;
            }
        }
        return 0;
    }

    /**
     * Compares this pattern's skull meta with the given skull meta.
     *
     * @param oneSkull   The first meta.
     * @param otherSkull The other meta.
     * @return The comparison result.
     */
    private static int compareSkulls(final SkullMeta oneSkull, final SkullMeta otherSkull) {
        if (oneSkull.getOwningPlayer() == null && otherSkull.getOwningPlayer() != null) {
            return -1;
        }
        if (oneSkull.getOwningPlayer() != null && otherSkull.getOwningPlayer() == null) {
            return 1;
        }
        final int cmp = oneSkull.getOwningPlayer().getUniqueId().compareTo(
                otherSkull.getOwningPlayer().getUniqueId());
        if (cmp != 0) {
            return cmp;
        }
        return 0;
    }

    /**
     * Compares potion effects.
     *
     * @param effectsA The first potion effects.
     * @param effectsB The second potion effects.
     * @return The comparison result.
     */
    private static int compareCustomEffects(final List<PotionEffect> effectsA, final List<PotionEffect> effectsB) {
        if (effectsA.size() != effectsB.size()) {
            return Integer.compare(effectsA.size(), effectsB.size());
        }
        for (int i = 0; i < effectsA.size(); i++) {
            final PotionEffect effectA = effectsA.get(i);
            final PotionEffect effectB = effectsB.get(i);
            final int cmpAmp = Integer.compare(effectA.getAmplifier(), effectB.getAmplifier());
            if (cmpAmp != 0) {
                return cmpAmp;
            }
            final int cmpDur = Integer.compare(effectA.getDuration(), effectB.getDuration());
            if (cmpDur != 0) {
                return cmpDur;
            }
            final int cmpCol = Integer.compare(effectA.getColor().asRGB(), effectB.getColor().asRGB());
            if (cmpCol != 0) {
                return cmpCol;
            }
            if (effectA.isAmbient() != effectB.isAmbient()) {
                return effectA.isAmbient() ? 1 : -1;
            }
            if (effectA.hasParticles() != effectB.hasParticles()) {
                return effectA.hasParticles() ? 1 : -1;
            }
        }
        return 0;
    }

    /**
     * Compares enchantments.
     *
     * @param mapA The first enchantment map.
     * @param mapB The second enchantment map.
     * @return The comparison result.
     */
    private static int compareEnchantments(final Map<Enchantment, Integer> mapA, final Map<Enchantment, Integer> mapB) {
        if (mapA.size() != mapB.size()) {
            return Integer.compare(mapA.size(), mapB.size());
        }
        for (final Enchantment ench : Enchantment.values()) {
            final int cmp = Integer.compare(mapA.getOrDefault(ench, 0),
                    mapB.getOrDefault(ench, 0));
            if (cmp != 0) {
                return cmp;
            }
        }
        return 0;
    }

    /**
     * Compares two firework effects.
     *
     * @param effectA The first firework effect.
     * @param effectB The second firework effect.
     * @return The comparison result.
     */
    private static int compareFireworkEffect(final FireworkEffect effectA, final FireworkEffect effectB) {
        if (effectA.getType() != effectB.getType()) {
            return effectA.getType().compareTo(effectB.getType());
        }
        if (effectA.hasFlicker() != effectB.hasFlicker()) {
            return effectA.hasFlicker() ? 1 : -1;
        }
        if (effectA.hasTrail() != effectB.hasTrail()) {
            return effectA.hasTrail() ? 1 : -1;
        }
        final List<Color> colorsA = effectA.getColors();
        final List<Color> colorsB = effectB.getColors();
        if (colorsA.size() != colorsB.size()) {
            return Integer.compare(colorsA.size(), colorsB.size());
        }
        for (int j = 0; j < colorsA.size(); j++) {
            final int cmpColor = Integer.compare(colorsA.get(j).asRGB(), colorsB.get(j).asRGB());
            if (cmpColor != 0) {
                return cmpColor;
            }
        }
        final List<Color> fadeColorsA = effectA.getFadeColors();
        final List<Color> fadeColorsB = effectB.getFadeColors();
        if (fadeColorsA.size() != fadeColorsB.size()) {
            return Integer.compare(fadeColorsA.size(), fadeColorsB.size());
        }
        for (int j = 0; j < fadeColorsA.size(); j++) {
            final int cmpColor = Integer.compare(fadeColorsA.get(j).asRGB(), fadeColorsB.get(j).asRGB());
            if (cmpColor != 0) {
                return cmpColor;
            }
        }
        return 0;
    }

    /**
     * Compares firework effects.
     *
     * @param effectsA The first firework effects.
     * @param effectsB The second firework effects.
     * @return The comparison result.
     */
    private static int compareFireworkEffects(final List<FireworkEffect> effectsA,
                                              final List<FireworkEffect> effectsB) {
        if (effectsA.size() != effectsB.size()) {
            return Integer.compare(effectsA.size(), effectsB.size());
        }
        for (int i = 0; i < effectsA.size(); i++) {
            final FireworkEffect effectA = effectsA.get(i);
            final FireworkEffect effectB = effectsB.get(i);
            final int cmpEffect = compareFireworkEffect(effectA, effectB);
            if (cmpEffect != 0) {
                return cmpEffect;
            }
        }
        return 0;
    }

    /**
     * Compares {@link PotionData}.
     *
     * @param dataA The first data.
     * @param dataB The second data.
     * @return The comparison result.
     */
    private static int comparePotionData(final PotionData dataA, final PotionData dataB) {
        if (dataA.getType() != dataB.getType()) {
            return dataA.getType().compareTo(dataB.getType());
        }
        if (dataA.isExtended() != dataB.isExtended()) {
            return dataA.isExtended() ? 1 : -1;
        }
        if (dataA.isUpgraded() != dataB.isUpgraded()) {
            return dataA.isUpgraded() ? 1 : -1;
        }
        return 0;
    }

    /**
     * The backend of the pattern.
     */
    private final ItemStack backend;

    /**
     * The item meta of the pattern.
     */
    private final ItemMeta meta;

    /**
     * Constructor.
     *
     * @param base The base item.
     */
    public ItemPattern(final ItemStack base) {
        if (base == null || base.getType() == Material.AIR) {
            throw new IllegalArgumentException("invalid item stack to create pattern from: " + base);
        }
        backend = base.clone();
        backend.setAmount(1);
        meta = backend.hasItemMeta() ? backend.getItemMeta() : null;
    }

    /**
     * Retrieves the {@link Material} of this pattern.
     *
     * @return The material of the pattern.
     */
    public Material getType() {
        return backend.getType();
    }

    /**
     * Checks whether or not the pattern contains the given enchantment.
     *
     * @param ench The enchantment.
     * @return True iff the pattern contains this enchantment.
     */
    public boolean hasEnchant(final Enchantment ench) {
        return meta != null && meta.hasEnchant(ench);
    }

    /**
     * Checks whether or not this item is banned for the use in storage.
     *
     * @return True iff it is banned.
     */
    public boolean isBanned() {
        // Shulker boxes are banned.
        switch (backend.getType()) {
            case WHITE_SHULKER_BOX:
            case ORANGE_SHULKER_BOX:
            case MAGENTA_SHULKER_BOX:
            case LIGHT_BLUE_SHULKER_BOX:
            case YELLOW_SHULKER_BOX:
            case LIME_SHULKER_BOX:
            case PINK_SHULKER_BOX:
            case GRAY_SHULKER_BOX:
            case SILVER_SHULKER_BOX:
            case CYAN_SHULKER_BOX:
            case PURPLE_SHULKER_BOX:
            case BLUE_SHULKER_BOX:
            case BROWN_SHULKER_BOX:
            case GREEN_SHULKER_BOX:
            case RED_SHULKER_BOX:
            case BLACK_SHULKER_BOX:
                return true;
            default:
                break;
        }

        // Storage cells are banned.
        StorageItems.CELL.reset(backend);
        if (StorageItems.CELL.isMatch()) {
            return true;
        }
        StorageItems.BACKPACK.reset(backend);
        if (StorageItems.BACKPACK.isMatch()) {
            return true;
        }

        // Ban block states that are not empty.
        if (meta != null) {
            if (meta instanceof BlockStateMeta) {
                return ((BlockStateMeta) meta).hasBlockState();
            }
        }

        return false;
    }

    /**
     * Creates an escaped display item for this pattern.
     *
     * @param amount The amount of the item that is present.
     * @return The escaped display item.
     */
    public ItemStack escapeItem(final long amount) {
        final ItemFactory fac = new ItemFactory(getSample()).lore("", "§eAnzahl: §5" + amount).ban();
        return fac.instance();
    }

    /**
     * Fetches a sample of this item pattern. The sample is of undefined amount.
     *
     * @return The sample.
     */
    public ItemStack getSample() {
        return backend.clone();
    }

    /**
     * Compares this pattern's meta with the given meta regarding the details.
     *
     * @param otherMeta The other meta.
     * @return The comparison result.
     */
    private int compareMetaDetail(final ItemMeta otherMeta) {
        final ItemMeta oneMeta = meta;

        if (oneMeta instanceof Repairable) {
            // Special case: Repairable items.
            final Repairable oneRepair = (Repairable) oneMeta;
            final Repairable otherRepair = (Repairable) otherMeta;
            if (oneRepair.hasRepairCost() != otherRepair.hasRepairCost()) {
                return oneRepair.hasRepairCost() ? 1 : -1;
            }
            if (oneRepair.hasRepairCost()) {
                final int cmp = Integer.compare(oneRepair.getRepairCost(), otherRepair.getRepairCost());
                if (cmp != 0) {
                    return cmp;
                }
            }
        }

        if (oneMeta instanceof EnchantmentStorageMeta) {
            // Special case: Enchantment storage.
            final EnchantmentStorageMeta oneStorage = (EnchantmentStorageMeta) oneMeta;
            final EnchantmentStorageMeta otherStorage = (EnchantmentStorageMeta) otherMeta;
            final int cmp = compareEnchantments(oneStorage.getStoredEnchants(), otherStorage.getStoredEnchants());
            if (cmp != 0) {
                return cmp;
            }
        }

        if (oneMeta instanceof SpawnEggMeta) {
            // Special case: Spawn eggs.
            final SpawnEggMeta oneEgg = (SpawnEggMeta) oneMeta;
            final SpawnEggMeta otherEgg = (SpawnEggMeta) otherMeta;
            final int cmp = oneEgg.getSpawnedType().compareTo(otherEgg.getSpawnedType());
            if (cmp != 0) {
                return cmp;
            }
        } else if (oneMeta instanceof SkullMeta) {
            // Special case: Skulls.
            final int cmpSkull = compareSkulls((SkullMeta) oneMeta, (SkullMeta) otherMeta);
            if (cmpSkull != 0) {
                return cmpSkull;
            }
        } else if (oneMeta instanceof MapMeta) {
            // Special case: Maps.
            final int cmpMap = compareMaps((MapMeta) oneMeta, (MapMeta) otherMeta);
            if (cmpMap != 0) {
                return cmpMap;
            }
        } else if (oneMeta instanceof LeatherArmorMeta) {
            // Special case: Leather armor.
            final LeatherArmorMeta oneArmor = (LeatherArmorMeta) oneMeta;
            final LeatherArmorMeta otherArmor = (LeatherArmorMeta) otherMeta;
            final int cmp = Integer.compare(oneArmor.getColor().asRGB(), otherArmor.getColor().asRGB());
            if (cmp != 0) {
                return cmp;
            }
        } else if (oneMeta instanceof KnowledgeBookMeta) {
            // Special case: Knowledge books.
            final int cmpKB = compareKnowledgeBooks((KnowledgeBookMeta) oneMeta, (KnowledgeBookMeta) otherMeta);
            if (cmpKB != 0) {
                return cmpKB;
            }
        } else if (oneMeta instanceof PotionMeta) {
            // Special case: Potions.
            final int cmpPotion = comparePotions((PotionMeta) oneMeta, (PotionMeta) otherMeta);
            if (cmpPotion != 0) {
                return cmpPotion;
            }
        } else if (oneMeta instanceof BookMeta) {
            // Special case: Books.
            final int cmpBook = compareBook((BookMeta) oneMeta, (BookMeta) otherMeta);
            if (cmpBook != 0) {
                return cmpBook;
            }
        } else if (oneMeta instanceof BannerMeta) {
            // Special case: Banners.
            final int cmpBanner = compareBanner((BannerMeta) oneMeta, (BannerMeta) otherMeta);
            if (cmpBanner != 0) {
                return cmpBanner;
            }
        } else if (oneMeta instanceof FireworkMeta) {
            // Special case: Firework.
            final int cmpFW = compareFireworks((FireworkMeta) oneMeta, (FireworkMeta) otherMeta);
            if (cmpFW != 0) {
                return cmpFW;
            }
        } else if (oneMeta instanceof FireworkEffectMeta) {
            // Special case: Firework effect.
            final int cmpFW = compareFireworkEffects((FireworkEffectMeta) oneMeta, (FireworkEffectMeta) otherMeta);
            if (cmpFW != 0) {
                return cmpFW;
            }
        }

        return 0;
    }

    /**
     * Compares this pattern's item meta with the given meta.
     *
     * @param otherMeta The other meta.
     * @return The comparison result.
     */
    private int compareMeta(final ItemMeta otherMeta) {
        final ItemMeta oneMeta = meta;

        // At this point, check similarity.
        if (Bukkit.getItemFactory().equals(oneMeta, otherMeta)) {
            return 0;
        }

        // Fourth criteria: Enchantments.
        final int cmpEnchant = compareEnchantments(oneMeta.getEnchants(), otherMeta.getEnchants());
        if (cmpEnchant != 0) {
            return cmpEnchant;
        }

        // Fifth criteria: Lore.
        if (otherMeta.hasLore() != oneMeta.hasLore()) {
            return oneMeta.hasLore() ? 1 : -1;
        }
        if (oneMeta.hasLore()) {
            // Fifth criteria (2): Amount of lines of lore.
            if (otherMeta.getLore().size() != oneMeta.getLore().size()) {
                return Integer.compare(oneMeta.getLore().size(), otherMeta.getLore().size());
            }

            // Fifth criteria (3): Lines of lore.
            for (int line = 0; line < oneMeta.getLore().size(); line++) {
                final String oneLine = oneMeta.getLore().get(line);
                final String otherLine = otherMeta.getLore().get(line);
                final int cmp = oneLine.compareTo(otherLine);
                if (cmp != 0) {
                    return cmp;
                }
            }
        }

        // Sixth criteria: Display names.
        if (otherMeta.hasDisplayName() != oneMeta.hasDisplayName()) {
            return oneMeta.hasDisplayName() ? 1 : -1;
        }
        if (oneMeta.hasDisplayName()) {
            // Sixth criteria (2): Actual display names.
            final String oneName = oneMeta.getDisplayName();
            final String otherName = otherMeta.getDisplayName();
            final int cmp = oneName.compareTo(otherName);
            if (cmp != 0) {
                return cmp;
            }
        }

        // Seventh criteria: Item flags.
        for (final ItemFlag flag : ItemFlag.values()) {
            if (oneMeta.hasItemFlag(flag) != otherMeta.hasItemFlag(flag)) {
                return oneMeta.hasItemFlag(flag) ? 1 : -1;
            }
        }

        // Eighth criteria: Classes.
        if (oneMeta.getClass() != otherMeta.getClass()) {
            return oneMeta.getClass().getName().compareTo(otherMeta.getClass().getName());
        }
        // At this point, the classes are the same.

        // Ninth criteria: Localized names.
        if (oneMeta.hasLocalizedName() != otherMeta.hasLocalizedName()) {
            return oneMeta.hasLocalizedName() ? 1 : -1;
        }
        if (oneMeta.hasLocalizedName()) {
            // Ninth criteria (2): Actual localized names.
            final String oneName = oneMeta.getLocalizedName();
            final String otherName = otherMeta.getLocalizedName();
            final int cmp = oneName.compareTo(otherName);
            if (cmp != 0) {
                return cmp;
            }
        }

        return compareMetaDetail(otherMeta);
    }

    /**
     * Checks whether two patterns are similar.
     *
     * @param other The other pattern.
     * @return True iff the patterns are similar.
     */
    private boolean isSimilar(final ItemPattern other) {
        return backend.isSimilar(other.backend);
    }

    @Override
    public int compareTo(final ItemPattern other) {
        // First criteria: Material.
        if (other.backend.getType() != backend.getType()) {
            return backend.getType().compareTo(other.backend.getType());
        }

        // Second criteria: Data byte.
        if (other.backend.getDurability() != backend.getDurability()) {
            return Integer.compare(backend.getDurability(), other.backend.getDurability());
        }

        // Third criteria: Item Meta.
        final boolean otherHasMeta = other.meta != null;
        final boolean hasMeta = meta != null;
        if (otherHasMeta != hasMeta) {
            return hasMeta ? 1 : -1;
        }

        // No meta: Everything else is already checked, they're the same.
        if (!hasMeta) {
            return 0;
        }

        // Compare the item meta.
        final int cmpMeta = compareMeta(other.meta);
        if (cmpMeta != 0) {
            return cmpMeta;
        }

        // Compare the internals of the item metas.
        final int cmpUnsafeInternals = Unsafe.compareMetaInternals(meta, other.meta);
        if (cmpUnsafeInternals != 0) {
            return cmpUnsafeInternals;
        }

        // Final criteria: Hash codes. This is not a good criteria, so this is our last resort.
        final int hcOne = hashCode();
        final int hcOther = other.hashCode();
        if (hcOne != hcOther) {
            return Integer.compare(hcOne, hcOther);
        }

        // Hash codes are equal but items are not equal.
        // throw new IllegalArgumentException("False equality for two items: " + backend + " / " + other.backend);
        // For some reason this happens with unsafe data, too.
        return 0;
    }

    @Override
    public int hashCode() {
        return backend.hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ItemPattern)) {
            return false;
        }
        return isSimilar((ItemPattern) obj);
    }
}
