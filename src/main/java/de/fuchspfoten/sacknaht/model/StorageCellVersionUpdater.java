package de.fuchspfoten.sacknaht.model;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.ArrayList;
import java.util.function.BiConsumer;

/**
 * Updates the version of data files of storage cells.
 */
public class StorageCellVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current version.
     */
    public static final int VERSION = 2;

    @Override
    public void accept(final Integer version, final DataFile dataFile) {
        if (version == 1) {
            // Add a set of members.
            dataFile.getStorage().set("members", new ArrayList<String>());
            // version++;
        }
    }
}
