package de.fuchspfoten.sacknaht.multiblock;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.item.InventoryTools;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.fuchslib.multiblock.SingleBlockMachine;
import de.fuchspfoten.fuchslib.ui.SelectorWheel;
import de.fuchspfoten.fuchslib.ui.SelectorWheel.SelectorWheelOption;
import de.fuchspfoten.itemtool.ItemStorage;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * A single workbench that is used as a machine.
 */
public class WorkbenchMachine extends SingleBlockMachine {

    /**
     * Finds cells from dropped items.
     *
     * @param entities         The entities containing the dropped items.
     * @param droppedCellItems Cell IDs mapped to their dropped items.
     * @param droppedCells     Cell IDs mapped to their cells.
     */
    private static void findCellsFromDropped(final Iterable<Entity> entities, final Map<UUID, Item> droppedCellItems,
                                             final Map<UUID, StorageCell> droppedCells) {
        for (final Entity entity : entities) {
            if (entity instanceof Item) {
                final Item item = (Item) entity;
                final ItemStack itemCurrent = item.getItemStack();
                if (itemCurrent == null) {
                    continue;
                }

                final String id = StorageCell.getIdOfCell(itemCurrent);
                if (id != null) {
                    final StorageCell sc = SacknahtPlugin.getSelf().getCellManager().getExistingCell(id);
                    if (sc != null) {
                        droppedCellItems.put(item.getUniqueId(), item);
                        droppedCells.put(item.getUniqueId(), sc);
                    }
                }
            }
        }
    }

    /**
     * The matcher for tanned leather.
     */
    private final ItemMatcher tannedLeatherMatcher;

    /**
     * The matcher for strings.
     */
    private final ItemMatcher stringMatcher;

    /**
     * The matcher for sewing kits.
     */
    private final ItemMatcher sewingKitMatcher;

    /**
     * Constructor.
     */
    public WorkbenchMachine() {
        Messenger.register("sacknaht.crafting.notSameSize");
        Messenger.register("sacknaht.crafting.notEmpty");
        Messenger.register("sacknaht.crafting.option.abort");
        Messenger.register("sacknaht.crafting.option.width");
        Messenger.register("sacknaht.crafting.option.depth");
        final ItemStorage storage = Bukkit.getServicesManager().load(ItemStorage.class);
        tannedLeatherMatcher = new ItemMatcher(storage.load("ingredient.leather"));
        sewingKitMatcher = new ItemMatcher(storage.load("tools.sewing_kit"));
        stringMatcher = new ItemMatcher(new ItemStack(Material.STRING));
    }

    /**
     * Performs a merge recipe.
     *
     * @param block    The node of the operation.
     * @param player   The player performing the operation.
     * @param decision The decision on which recipe to perform. -1: undecided, 0: width, otherwise: depth.
     */
    private void performMergeRecipe(final Block block, final Player player, final int decision) {
        // Find nearby dropped items.
        final Collection<Entity> entities = block.getWorld().getNearbyEntities(
                block.getLocation().add(0.5, 0.5, 0.5), 0.75, 1.0, 0.75);

        // Find all storage cells in the items.
        final Map<UUID, Item> droppedCellItems = new HashMap<>();
        final Map<UUID, StorageCell> droppedCells = new HashMap<>();
        findCellsFromDropped(entities, droppedCellItems, droppedCells);

        if (droppedCells.size() == 2) {
            final List<UUID> ids = new ArrayList<>(droppedCells.keySet());
            final UUID droppedIdA = ids.get(0);
            final UUID droppedIdB = ids.get(1);
            final StorageCell cellA = droppedCells.get(droppedIdA);
            final StorageCell cellB = droppedCells.get(droppedIdB);
            if (cellA.isBackpack() || cellB.isBackpack() || cellA == cellB) {
                return;
            }
            if (!cellA.canManage(player) || !cellB.canManage(player)) {
                return;
            }

            // Need 8 leather, 8 string -> combine chosen sizes.
            final int tannedLeatherPresent = InventoryTools.findInDropped(entities, 0,
                    tannedLeatherMatcher);
            final int stringPresent = InventoryTools.findInDropped(entities, 0, stringMatcher);
            if (tannedLeatherPresent < 8 || stringPresent < 8) {
                return;
            }

            if (cellA.getMaxItemsPerSlot() != cellB.getMaxItemsPerSlot()
                    || cellA.getMaxSlots() != cellB.getMaxSlots()) {
                Messenger.send(player, "sacknaht.crafting.notSameSize");
                return;
            }

            if (cellA.getRealUsedSlots() != 0 || cellB.getRealUsedSlots() != 0) {
                Messenger.send(player, "sacknaht.crafting.notEmpty");
                return;
            }

            if (decision == -1) {
                final SelectorWheel selectorWheel = new SelectorWheel();
                selectorWheel.cancelOption(new SelectorWheelOption(Messenger.getFormat(
                        "sacknaht.crafting.option.abort"), l -> {
                }));
                selectorWheel.options(
                        new SelectorWheelOption(Messenger.getFormat("sacknaht.crafting.option.width"),
                                l -> performMergeRecipe(block, player, 0)),
                        new SelectorWheelOption(Messenger.getFormat("sacknaht.crafting.option.depth"),
                                l -> performMergeRecipe(block, player, 1))
                );
                selectorWheel.show(player);
            } else {
                InventoryTools.findInDropped(entities, 8, tannedLeatherMatcher);
                InventoryTools.findInDropped(entities, 8, stringMatcher);
                if (decision == 0) {
                    // Width.
                    cellA.setMaxSlots(cellA.getMaxSlots() + cellB.getMaxSlots());
                } else {
                    // Depth.
                    cellA.setMaxItemsPerSlot(cellA.getMaxItemsPerSlot() + cellB.getMaxItemsPerSlot());
                }

                // Remove cellB and make it unusable.
                cellB.setMaxSlots(0);
                cellB.setMaxItemsPerSlot(0);
                droppedCellItems.get(droppedIdB).remove();

                // Show some particle effects.
                block.getWorld().spawnParticle(Particle.END_ROD, block.getLocation().add(0.5, 1.0, 0.5),
                        32, 0.3, 0.3, 0.3, 1.0);
            }
        }
    }

    @Override
    public void onNodeInteract(final Block block, final PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        // Check for the item in hand.
        final ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        sewingKitMatcher.reset(itemInHand);
        if (!sewingKitMatcher.isMatch()) {
            return;
        }

        // Find nearby dropped items.
        final Collection<Entity> entities = block.getWorld().getNearbyEntities(
                block.getLocation().add(0.5, 0.5, 0.5), 0.75, 1.0, 0.75);

        // Find all storage cells in the items.
        final Map<UUID, Item> droppedCellItems = new HashMap<>();
        final Map<UUID, StorageCell> droppedCells = new HashMap<>();
        findCellsFromDropped(entities, droppedCellItems, droppedCells);

        // Possible recipes:
        // - Single backpack: Upgrade -> !backpack + fixed size increase.
        // - Two cells of same size: Depth upgrade.
        // - Two cells of same size: Width upgrade.
        // Show a selection wheel for the latter two.

        if (droppedCells.size() == 1) {
            final UUID droppedId = droppedCells.keySet().stream().findAny().orElse(null);
            final StorageCell cell = droppedCells.get(droppedId);
            if (!cell.isBackpack()) {
                return;
            }
            if (!cell.canManage(event.getPlayer())) {
                return;
            }

            // Need 4 tanned leather, 4 string -> 100/6400 storage cell.
            final int tannedLeatherPresent = InventoryTools.findInDropped(entities, 0,
                    tannedLeatherMatcher);
            final int stringPresent = InventoryTools.findInDropped(entities, 0, stringMatcher);
            if (stringPresent < 4 || tannedLeatherPresent < 4) {
                return;
            }
            InventoryTools.findInDropped(entities, 4, tannedLeatherMatcher);
            InventoryTools.findInDropped(entities, 4, stringMatcher);

            // Upgrade the cell.
            cell.setBackpack(false);
            cell.setMaxItemsPerSlot(Math.max(6400, cell.getMaxItemsPerSlot()));
            cell.setMaxSlots(Math.max(100, cell.getMaxSlots()));
            droppedCellItems.get(droppedId).setItemStack(cell.getItem());

            // Show some particle effects.
            block.getWorld().spawnParticle(Particle.END_ROD, block.getLocation().add(0.5, 1.0, 0.5),
                    32, 0.3, 0.3, 0.3, 1.0);
        } else if (droppedCells.size() == 2) {
            performMergeRecipe(block, event.getPlayer(), -1);
        }
    }
}
