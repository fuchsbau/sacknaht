package de.fuchspfoten.sacknaht.multiblock;

import de.fuchspfoten.fuchslib.item.InventoryTools;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.fuchslib.multiblock.SingleBlockMachine;
import de.fuchspfoten.itemtool.ItemStorage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Cauldron;

import java.util.Collection;

/**
 * A single cauldron that is used as a machine.
 */
public class CauldronMachine extends SingleBlockMachine {

    /**
     * The tanned leather that can be produced.
     */
    private final ItemStack tannedLeather;

    /**
     * The matcher for the cooking spoon.
     */
    private final ItemMatcher cookingSpoonMatcher;

    /**
     * The raw leather matcher.
     */
    private final ItemMatcher rawLeatherMatcher;

    /**
     * The matcher for gallnuts.
     */
    private final ItemMatcher gallnutMatcher;

    /**
     * The matcher for oak bark.
     */
    private final ItemMatcher barkOakMatcher;

    /**
     * The matcher for spruce bark.
     */
    private final ItemMatcher barkSpruceMatcher;

    /**
     * Constructor.
     */
    public CauldronMachine() {
        final ItemStorage storage = Bukkit.getServicesManager().load(ItemStorage.class);
        tannedLeather = storage.load("ingredient.leather");
        rawLeatherMatcher = new ItemMatcher(new ItemStack(Material.LEATHER));
        cookingSpoonMatcher = new ItemMatcher(storage.load("tools.cooking_spoon"));
        gallnutMatcher = new ItemMatcher(storage.load("ingredient.gallnut"));
        barkOakMatcher = new ItemMatcher(storage.load("ingredient.bark.oak"));
        barkSpruceMatcher = new ItemMatcher(storage.load("ingredient.bark.spruce"));
    }

    @Override
    public void onNodeInteract(final Block block, final PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        // Check the water level.
        final Cauldron cauldron = (Cauldron) block.getState().getData();
        if (!cauldron.isFull()) {
            return;
        }

        // Check for the item in hand.
        final ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        cookingSpoonMatcher.reset(itemInHand);
        if (!cookingSpoonMatcher.isMatch()) {
            return;
        }

        // Find nearby dropped items.
        final Collection<Entity> entities = block.getWorld().getNearbyEntities(
                block.getLocation().add(0.5, 0.5, 0.5), 0.75, 1.0, 0.75);
        final int leatherPresent = InventoryTools.findInDropped(entities, 0, rawLeatherMatcher);
        final int ingredientsPresent = InventoryTools.findInDropped(entities, 0, barkOakMatcher,
                barkSpruceMatcher, gallnutMatcher);
        if (leatherPresent < 1 || ingredientsPresent < 3) {
            return;
        }
        InventoryTools.findInDropped(entities, 1, rawLeatherMatcher);
        InventoryTools.findInDropped(entities, 3, barkOakMatcher, barkSpruceMatcher, gallnutMatcher);

        // It worked, produce the result.
        block.getWorld().dropItem(block.getLocation().add(0.5, 1.5, 0.5), tannedLeather.clone());
        block.getWorld().spawnParticle(Particle.PORTAL, block.getLocation().add(0.5, 1.0, 0.5), 64,
                0.3, 0.3, 0.3, 1.0);
    }
}
