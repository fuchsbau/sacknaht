package de.fuchspfoten.sacknaht.multiblock;

import de.fuchspfoten.fuchslib.item.InventoryTools;
import de.fuchspfoten.fuchslib.item.ItemMatcher;
import de.fuchspfoten.fuchslib.multiblock.SingleBlockMachine;
import de.fuchspfoten.itemtool.ItemStorage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;

/**
 * A single anvil that is used as a machine.
 */
public class AnvilMachine extends SingleBlockMachine {

    /**
     * The amount of iron nuggets needed for a sewing kit.
     */
    private static final int IRON_NUGGETS_NEEDED = 6;

    /**
     * The matcher for the smithing hammer.
     */
    private final ItemMatcher smithingHammerMatcher;

    /**
     * The matcher for item nuggets.
     */
    private final ItemMatcher ironNuggetMatcher;

    /**
     * The sewing kit that can be produced.
     */
    private final ItemStack sewingKit;

    /**
     * Constructor.
     */
    public AnvilMachine() {
        final ItemStorage storage = Bukkit.getServicesManager().load(ItemStorage.class);
        smithingHammerMatcher = new ItemMatcher(storage.load("tools.smithing_hammer"));
        ironNuggetMatcher = new ItemMatcher(new ItemStack(Material.IRON_NUGGET));
        sewingKit = storage.load("tools.sewing_kit");
    }

    @Override
    public void onNodeInteract(final Block block, final PlayerInteractEvent event) {
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) {
            return;
        }

        // Check for the item in hand.
        final ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        smithingHammerMatcher.reset(itemInHand);
        if (!smithingHammerMatcher.isMatch()) {
            return;
        }

        // Find nearby dropped items.
        final Collection<Entity> entities = block.getWorld().getNearbyEntities(
                block.getLocation().add(0.5, 1.5, 0.5), 0.75, 0.75, 0.75);
        final int nuggetsPresent = InventoryTools.findInDropped(entities, 0, ironNuggetMatcher);
        if (nuggetsPresent < IRON_NUGGETS_NEEDED) {
            return;
        }
        InventoryTools.findInDropped(entities, IRON_NUGGETS_NEEDED, ironNuggetMatcher);

        // It worked, produce the result.
        block.getWorld().dropItem(block.getLocation().add(0.5, 1.5, 0.5), sewingKit.clone());
        event.getPlayer().playSound(block.getLocation(), Sound.BLOCK_ANVIL_USE, 1.0f, 0.0f);
    }
}
