package de.fuchspfoten.sacknaht.multiblock;

import de.fuchspfoten.fuchslib.multiblock.Machine;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.StorageCell;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A machine able to read {@link de.fuchspfoten.sacknaht.model.StorageCell}s.
 */
@RequiredArgsConstructor
public class ReaderMachine implements Machine {

    /**
     * The block which is accessed (enchantment table).
     */
    private final Block accessBlock;

    /**
     * The block which contains the data (chest).
     */
    private final Block chestBlock;

    /**
     * Checks whether or not this machine is valid.
     *
     * @return {@code true} iff this machine is valid.
     */
    private boolean isMachineValid() {
        if (chestBlock.getType() != Material.CHEST) {
            return false;
        }
        return accessBlock.getType() == Material.ENCHANTMENT_TABLE;
    }

    @Override
    public Collection<Block> getNodes() {
        return Collections.singleton(accessBlock);
    }

    @Override
    public void onNodeInteract(final Block block, final PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        // Perform a sanity check of the machine first.
        if (!isMachineValid()) {
            return;
        }

        final Chest chest = (Chest) chestBlock.getState();
        final Set<StorageCell> cells = Arrays.stream(chest.getBlockInventory().getContents())
                .filter(Objects::nonNull)
                .map(StorageCell::getIdOfCell)
                .filter(Objects::nonNull)
                .map(SacknahtPlugin.getSelf().getCellManager()::getExistingCell)
                .filter(Objects::nonNull)
                .filter(sc -> sc.hasPermission(event.getPlayer().getUniqueId()))
                .collect(Collectors.toSet());
        SacknahtPlugin.getSelf().getInterfaceManager().open(cells, event.getPlayer());
        event.setCancelled(true);
    }
}
