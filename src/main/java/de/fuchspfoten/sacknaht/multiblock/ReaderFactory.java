package de.fuchspfoten.sacknaht.multiblock;

import de.fuchspfoten.fuchslib.multiblock.Machine;
import de.fuchspfoten.fuchslib.multiblock.MachineFactory;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

/**
 * A factory for creating {@link de.fuchspfoten.sacknaht.multiblock.ReaderMachine}s.
 */
public class ReaderFactory implements MachineFactory {

    /**
     * Finds an adjacent chest.
     *
     * @param clickedBlock The source block.
     * @return The chest.
     */
    private static Block findChest(final Block clickedBlock) {
        final BlockFace[] faces = new BlockFace[]{BlockFace.DOWN, BlockFace.NORTH, BlockFace.WEST, BlockFace.SOUTH,
                BlockFace.EAST};
        for (final BlockFace face : faces) {
            final Block adjacent = clickedBlock.getRelative(face);
            if (adjacent.getType() == Material.CHEST) {
                return adjacent;
            }
        }
        return null;
    }

    @Override
    public Machine tryCreateAt(final Block block) {
        if (block.getType() == Material.ENCHANTMENT_TABLE) {
            final Block chest = findChest(block);
            if (chest != null) {
                return new ReaderMachine(block, chest);
            }
        }
        return null;
    }
}
