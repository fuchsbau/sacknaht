package de.fuchspfoten.sacknaht.data;

import de.fuchspfoten.sacknaht.model.ItemPattern;

/**
 * A sorted storage for cell contents.
 */
public interface ContentStorage {

    /**
     * Adds the given amount to the entry with the given pattern. If the pattern has no entry, one is created.
     *
     * @param pattern The pattern.
     * @param amount  The amount to add.
     */
    void add(final ItemPattern pattern, final long amount);

    /**
     * Retrieves the element stored for the given pattern.
     *
     * @param pattern The pattern.
     * @return The stored element.
     */
    ContentIterator get(final ItemPattern pattern);

    /**
     * Gets an iterator at the given position.
     *
     * @param pos The position.
     * @return The iterator.
     */
    ContentIterator get(final int pos);

    /**
     * Merges the given source into this storage.
     *
     * @param source The source.
     */
    void merge(final ContentStorage source);

    /**
     * Associates the given item pattern with the given amount of items.
     *
     * @param pattern The pattern.
     * @param amount  The amount.
     */
    void put(final ItemPattern pattern, final long amount);

    /**
     * Removes the given pattern from the storage.
     *
     * @param pattern The pattern.
     */
    void remove(final ItemPattern pattern);

    /**
     * Returns the size of the storage.
     *
     * @return The size of the storage.
     */
    int size();
}
