package de.fuchspfoten.sacknaht.data;

import de.fuchspfoten.sacknaht.model.ItemPattern;

/**
 * Represents a content iterator.
 */
public interface ContentIterator {

    /**
     * Obtains the item amount for the current entry.
     *
     * @return The amount.
     */
    long getAmount();

    /**
     * Obtains the item pattern for the current entry.
     *
     * @return The pattern.
     */
    ItemPattern getPattern();

    /**
     * Returns the next content iterator in the sequence.
     * The sequence may be broken when changing the underlying tree.
     *
     * @return The next content iterator or null if there is none.
     */
    ContentIterator next();
}
