package de.fuchspfoten.sacknaht.data;

import de.fuchspfoten.sacknaht.model.ItemPattern;
import lombok.Getter;

/**
 * A {@link ContentStorage} implemented using AVL trees.
 */
public class AVLContentStorage implements ContentStorage {

    /**
     * The root of the AVL tree.
     */
    private AVLNode root;

    /**
     * The size is tracked externally.
     */
    private int size;

    /**
     * Constructor.
     */
    public AVLContentStorage() {
        root = AVLNode.SENTINEL;
        size = 0;
    }

    @Override
    public void add(final ItemPattern pattern, final long amount) {
        if (pattern == null) {
            throw new IllegalArgumentException("pattern may not be null");
        }
        root = root.insert(this, root, pattern, amount, true);
    }

    @Override
    public ContentIterator get(final ItemPattern pattern) {
        if (pattern == null) {
            throw new IllegalArgumentException("pattern may not be null");
        }
        return root.search(pattern);
    }

    @Override
    public ContentIterator get(final int pos) {
        if (pos >= size || pos < 0) {
            throw new IllegalArgumentException("invalid position: " + pos);
        }
        return root.get(pos);
    }

    @Override
    public void merge(final ContentStorage source) {
        // TODO: O(n) implementation?
        if (source.size() == 0) {
            return;
        }

        ContentIterator cursor = source.get(0);
        while (cursor != null) {
            root = root.insert(this, root, cursor.getPattern(), cursor.getAmount(), true);
            cursor = cursor.next();
        }
    }

    @Override
    public void put(final ItemPattern pattern, final long amount) {
        if (pattern == null) {
            throw new IllegalArgumentException("pattern may not be null");
        }
        root = root.insert(this, root, pattern, amount, false);
    }

    @Override
    public void remove(final ItemPattern pattern) {
        if (pattern == null) {
            throw new IllegalArgumentException("pattern may not be null");
        }
        root = root.remove(this, pattern);
        root.parent = AVLNode.SENTINEL;
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * A node in the AVL tree.
     */
    public static class AVLNode implements ContentIterator {

        /**
         * The sentinel node.
         */
        public static final AVLNode SENTINEL;

        static {
            //noinspection AnonymousInnerClassWithTooManyMethods,InnerClassTooDeeplyNested
            SENTINEL = new AVLNode(null, null, 0) {
                @Override
                protected AVLNode minimal() {
                    // May be called: But minimal of this "subtree" is null.
                    return null;
                }

                @Override
                public AVLNode search(final ItemPattern query) {
                    // May be called: But query is not found in "subtree".
                    return null;
                }

                @Override
                public AVLNode get(final int position) {
                    // May be called: But position is not found in "subtree".
                    return null;
                }

                @Override
                protected void setParent(final AVLNode newParent) {
                    // No-Op: Sentinel parent shall not be changed.
                }

                @Override
                public AVLNode insert(final AVLContentStorage tree, final AVLNode from, final ItemPattern pattern,
                                      final long amount, final boolean add) {
                    // Increase the size as we created a new node. This is the only place where new nodes are created.
                    tree.size++;
                    return new AVLNode(from, pattern, amount);
                }

                @Override
                public AVLNode remove(final AVLContentStorage tree, final ItemPattern which) {
                    // Removing from this "subtree" changes nothing.
                    return this;
                }
            };
            SENTINEL.parent = SENTINEL;
            SENTINEL.left = SENTINEL;
            SENTINEL.right = SENTINEL;
            SENTINEL.size = 0;
            SENTINEL.height = (byte) 0;
        }

        /**
         * The pattern stored in the node.
         */
        private @Getter ItemPattern pattern;

        /**
         * The amount stored in the node.
         */
        private @Getter long amount;

        /**
         * The parent node.
         */
        private @Getter AVLNode parent;

        /**
         * The left child.
         */
        private @Getter AVLNode left;

        /**
         * The right child.
         */
        private @Getter AVLNode right;

        /**
         * The number of nodes the subtree that is rooted in this node contains.
         */
        private int size;

        /**
         * The height of the subtree rooted in this node. The maximum value (127) is enough for at least 2^32 entries.
         */
        private byte height;

        /**
         * Constructor.
         *
         * @param parent  The parent node.
         * @param pattern The item pattern this node will store.
         * @param amount  The amount of the item that is stored in this node.
         */
        private AVLNode(final AVLNode parent, final ItemPattern pattern, final long amount) {
            this.parent = parent;
            this.pattern = pattern;
            this.amount = amount;
            left = SENTINEL;
            right = SENTINEL;
            size = 1;
            height = (byte) 1;
        }

        /**
         * Gets the node at the given position.
         *
         * @param position The position, between 0 and the size of the content storage.
         * @return The node, or null if there is none.
         */
        public AVLNode get(final int position) {
            final int numLeft = left.size;
            if (position < numLeft) {
                // We can continue with unchanged position.
                return left.get(position);
            }
            if (position > numLeft) {
                // We now base 0 at the minimum value not in the left subtree nor at this node. Thus we do no longer
                //  consider numLeft+1 items to the left.
                return right.get(position - numLeft - 1);
            }
            return this;
        }

        /**
         * Inserts the given item pattern and the given amount as a node in the subtree rooted at this node.
         *
         * @param tree    The tree in which a node is inserted.
         * @param from    The node where the request originates from, may be sentinel.
         * @param pattern The pattern.
         * @param amount  The amount.
         * @param add     Whether amounts are added (true) or replaced (false).
         * @return The new root of the subtree at this node (due to rotation for example).
         */
        public AVLNode insert(final AVLContentStorage tree, final AVLNode from, final ItemPattern pattern,
                              final long amount, final boolean add) {
            final int cmp = this.pattern.compareTo(pattern);

            // Is this already the desired node?
            if (cmp == 0) {
                // Only update this entry.
                if (add) {
                    this.amount += amount;
                } else {
                    this.amount = amount;
                }
                return this;
            }

            if (cmp < 0) {
                // Self is less than desired: Insert on the right.
                right = right.insert(tree, this, pattern, amount, add);
            } else {
                // Self is greater than desired: Insert on the left.
                left = left.insert(tree, this, pattern, amount, add);
            }
            // Invariant: Left and Right are ok-heavy or balanced.

            // At this point, we inserted the node into the tree and rebalancing for subtrees right and left is
            //  completed, if it was needed.
            size = left.size + right.size + 1;
            height = (byte) (Math.max(left.height, right.height) + 1);
            final byte balance = (byte) (left.height - right.height);

            if (balance > 1) {
                // Left is not sentinel: Otherwise, balance is <= 0.
                final int cmpLeft = pattern.compareTo(left.pattern);
                if (cmpLeft < 0) {
                    // The inserted node is a child of z. x is the current node.
                    // A or D is "too deep", we need to pull both up.
                    //       x               y
                    //      / \             / \
                    //     y   B           /   \
                    //    / \      ==>    /     \
                    //   z   C           z       x
                    //  / \             / \     / \
                    // A   D           A   D   C   B
                    return rotateRight();
                }
                if (cmpLeft > 0) {
                    // The inserted node is a child of z. x is the current node.
                    // A or D is "too deep", we need to pull both up.
                    //       x               x               z
                    //      / \             / \             / \
                    //     y   B           z   B           /   \
                    //    / \      ==>    / \      ==>    /     \
                    //   C   z           y   D           y       x
                    //      / \         / \             / \     / \
                    //     A   D       C   A           C   A   D   B
                    left = left.rotateLeft();
                    // x height might be out of sync. But next rotation fixes this.
                    return rotateRight();
                }
                // cmpLeft == 0: In this case, height(left) is now 1. But as b = h(l) - h(r) > 1, this is impossible.
            } else if (balance < -1) {
                // Right is not sentinel: Otherwise, balance is >= 0.
                final int cmpRight = pattern.compareTo(right.pattern);
                if (cmpRight > 0) {
                    // The inserted node is a child of z. x is the current node.
                    // C or D is "too deep", we need to pull both up.
                    //   x                 y
                    //  / \               / \
                    // A   y             /   \
                    //    / \    ==>    /     \
                    //   B   z         x       z
                    //      / \       / \     / \
                    //     C   D     A   B   C   D
                    return rotateLeft();
                }
                if (cmpRight < 0) {
                    // The inserted node is a child of z. x is the current node.
                    // C or D is "too deep", we need to pull both up.
                    //   x               x                 z
                    //  / \             / \               / \
                    // A   y           A   z             /   \
                    //    / \    ==>      / \    ==>    /     \
                    //   z   B           C   y         x       y
                    //  / \                 / \       / \     / \
                    // C   D               D   B     A   C   D   B
                    right = right.rotateRight();
                    // x height might be out of sync. But next rotation fixes this.
                    return rotateLeft();
                }
                // cmpRight == 0: In this case, height(right) is now 1. But as b = h(l) - h(r) < -1, this is impossible.
            }

            // We are still the root at this point.
            return this;
        }

        /**
         * Removes the node with the given pattern from the subtree rooted at this node.
         *
         * @param tree  The tree in which a node is removed.
         * @param which The pattern that is removed.
         * @return The new root of the subtree at this node (due to rotation for example).
         */
        public AVLNode remove(final AVLContentStorage tree, final ItemPattern which) {
            final int cmp = pattern.compareTo(which);

            // Is this already the desired node?
            if (cmp == 0) {
                if (left == SENTINEL || right == SENTINEL) {
                    // One of the children is non-existent. We just remove this node and return the existing child
                    //  (or any child, if both do not exist).
                    // This happens at some point: If we do not take this way out, we remove from our right child. This
                    //  recursion must terminate because the right subtree is getting smaller.
                    tree.size--;
                    return left == SENTINEL ? right : left;
                }

                // At this point we have a two-child node.
                final AVLNode tmp = right.minimal();
                pattern = tmp.pattern;
                amount = tmp.amount;

                // Remove the node that we swapped into this node.
                //  This node now contains tmp.pattern, but we only remove from the right subtree, so this node is left
                //  alone.
                right = right.remove(tree, tmp.pattern);
                right.parent = this;

                // Update the size as the children changed.
                size = left.size + right.size + 1;
            }

            if (cmp < 0) {
                // Self is less than desired: Remove from the right subtree.
                right = right.remove(tree, which);
                right.parent = this;
            } else if (cmp > 0) {
                // Self is greater than desired: Remove from the left subtree.
                left = left.remove(tree, which);
                left.parent = this;
            }
            // Invariant: Left and Right are ok-heavy or balanced.

            // At this point, we removed the entry from this subtree. Now we need to rebalance the subtree.
            size = left.size + right.size + 1;
            height = (byte) (Math.max(left.height, right.height) + 1);
            final byte balance = (byte) (left.height - right.height);

            if (balance > 1) {
                // Left is not sentinel: Otherwise, balance is <= 0.
                final byte leftBalance = (byte) (left.left.height - left.right.height);
                if (leftBalance >= 0) {
                    //        x               y
                    //       / \             / \
                    //      y   E           /   \
                    //     / \             z     x
                    //    /   \     ==>   / \   / \
                    //   z     w         A   B w   E
                    //  / \   / \             / \
                    // A   B C   D           C   D
                    return rotateRight();
                }
                //
                //     x               x               z
                //    / \             / \             / \
                //   y   D           z   D           /   \
                //  / \      ==>    / \      ==>    /     \
                // A   z           y   C           y       x
                //    / \         / \             / \     / \
                //   B   C       A   B           A   B   C   D
                left = left.rotateLeft();
                // x height might be not in sync, but next rotation fixes this.
                return rotateRight();
            }
            if (balance < -1) {
                // Right is not sentinel: Otherwise, balance is >= 0.
                final byte rightBalance = (byte) (right.left.height - right.right.height);
                if (rightBalance <= 0) {
                    //    x                   y
                    //   / \                 / \
                    //  E   y               /   \
                    //     / \             x     w
                    //    /   \     ==>   / \   / \
                    //   z     w         E   z C   D
                    //  / \   / \           / \
                    // A   B C   D         A   B
                    return rotateLeft();
                }
                //   x             x                 z
                //  / \           / \               / \
                // C   y         C   z             /   \
                //    / \   ==>     / \    ==>    /     \
                //   z   D         A   y         x       y
                //  / \               / \       / \     / \
                // A   B             B   D     C   A   B   D
                right = right.rotateRight();
                // x height might be not in sync, but next rotation fixes this.
                return rotateLeft();
            }

            return this;
        }

        /**
         * Searches for the given pattern.
         *
         * @param query The pattern.
         * @return The node containing the pattern, null if there is none.
         */
        public AVLNode search(final ItemPattern query) {
            final int cmp = pattern.compareTo(query);
            if (cmp == 0) {
                // Self is the query.
                return this;
            }
            if (cmp < 0) {
                // Self is less than query.
                return right.search(query);
            }
            // Self is greater than or equal to the query. As equal must be covered by the similarity check, self is
            //  greater than the query.
            return left.search(query);
        }

        /**
         * Retrieves the minimum node of this subtree.
         *
         * @return The minimum node.
         */
        protected AVLNode minimal() {
            if (left == SENTINEL) {
                // We have no left children: We are the minimal node.
                return this;
            }
            // We have a left child which is smaller: Its minimum is our minimum.
            return left.minimal();
        }

        /**
         * Changes the parent of this node. This method is a no-op for sentinels.
         *
         * @param newParent The new parent.
         */
        protected void setParent(final AVLNode newParent) {
            parent = newParent;
        }

        @Override
        public AVLNode next() {
            if (right == SENTINEL) {
                // We need to find the first ancestor which we reach over a "right-bending" edge:
                //       ...            ...
                //       /                \
                //      y                  y
                //     / \       OR       / \
                //    x  ...             x  ...
                //   / \                / \
                // ... ...            ... ...
                // The cursor is node x. We want to return node y, as it is minimally greater. If our parent is the
                // sentinel, we are at the root, and we can return null.
                AVLNode cursor = this;
                do {
                    if (cursor.parent.left == cursor) {
                        return cursor.parent;
                    }
                    cursor = cursor.parent;
                } while (cursor != SENTINEL);
                return null;
            }
            // We have a greater child: Take the minimal greater node.
            return right.minimal();
        }

        /**
         * Rotates this node left:
         * <pre>
         *       x           y
         *      /|          /|
         *     / |         / |
         *    y  C   <==  A  x
         *   /|             /|
         *  / |            / |
         * A  B           B  C
         * </pre>
         * with y being this node. x and y may not be sentinel nodes.
         *
         * @return The new root node x.
         */
        private AVLNode rotateLeft() {
            // Change the parents of x and y. Both are no sentinel, so this is fine.
            final AVLNode oldParent = parent;
            parent = right;
            right.parent = oldParent;

            // Rotate left.
            final AVLNode newRoot = right;
            right = newRoot.left;
            newRoot.left = this;
            size = left.size + right.size + 1;
            newRoot.size = newRoot.left.size + newRoot.right.size + 1;

            // Update the heights.
            height = (byte) (Math.max(left.height, right.height) + 1);
            newRoot.height = (byte) (Math.max(newRoot.left.height, newRoot.right.height) + 1);

            // Change the parent of subtree B if it is not a sentinel.
            right.setParent(this);

            return newRoot;
        }

        /**
         * Rotates this node right:
         * <pre>
         *       x           y
         *      /|          /|
         *     / |         / |
         *    y  C   ==>  A  x
         *   /|             /|
         *  / |            / |
         * A  B           B  C
         * </pre>
         * with x being this node. x and y may not be sentinel nodes.
         *
         * @return The new root node y.
         */
        private AVLNode rotateRight() {
            // Change the parents of x and y. Both are no sentinel, so this is fine.
            final AVLNode oldParent = parent;
            parent = left;
            left.parent = oldParent;

            // Rotate right.
            final AVLNode newRoot = left;
            left = newRoot.right;
            newRoot.right = this;
            size = left.size + right.size + 1;
            newRoot.size = newRoot.left.size + newRoot.right.size + 1;

            // Update the heights.
            height = (byte) (Math.max(left.height, right.height) + 1);
            newRoot.height = (byte) (Math.max(newRoot.left.height, newRoot.right.height) + 1);

            // Change the parent of subtree B if it is not a sentinel.
            left.setParent(this);

            return newRoot;
        }
    }
}
