package de.fuchspfoten.sacknaht;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.fuchslib.multiblock.MachineRegistry;
import de.fuchspfoten.sacknaht.model.MemberManager;
import de.fuchspfoten.sacknaht.model.StorageCellVersionUpdater;
import de.fuchspfoten.sacknaht.modules.BackpackOpenModule;
import de.fuchspfoten.sacknaht.modules.CellInitializerModule;
import de.fuchspfoten.sacknaht.modules.CraftingDropsModule;
import de.fuchspfoten.sacknaht.modules.MECommandModule;
import de.fuchspfoten.sacknaht.modules.MEInterfaceModule;
import de.fuchspfoten.sacknaht.multiblock.AnvilMachine;
import de.fuchspfoten.sacknaht.multiblock.CauldronMachine;
import de.fuchspfoten.sacknaht.multiblock.ReaderFactory;
import de.fuchspfoten.sacknaht.multiblock.WorkbenchMachine;
import de.fuchspfoten.sacknaht.search.SearchManager;
import de.fuchspfoten.sacknaht.ui.InterfaceManager;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * The main plugin class.
 */
public class SacknahtPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static SacknahtPlugin self;

    /**
     * The data folder for storage cells.
     */
    private @Getter File storageFolder;

    /**
     * The cell manager.
     */
    private @Getter CellManager cellManager;

    /**
     * The search manager.
     */
    private @Getter SearchManager searchManager;

    /**
     * The interface manager.
     */
    private @Getter InterfaceManager interfaceManager;

    /**
     * The member management conversation manager.
     */
    private @Getter MemberManager memberManager;

    /**
     * The worlds in which readers may be used.
     */
    private Set<String> allowedReaderWorlds;

    /**
     * The worlds in special crafting drops occur.
     */
    private Set<String> allowedDropWorlds;

    /**
     * Checks whether a cell with the given ID exists.
     *
     * @param id The cell ID.
     * @return Whether a cell with the given ID exists.
     */
    public boolean cellExists(final String id) {
        final File dataFile = new File(storageFolder, id + ".yml");
        return dataFile.exists();
    }

    /**
     * Fetches the data file for the given cell id.
     *
     * @param id The cell ID.
     * @return The data file.
     */
    public DataFile getCellDataFile(final String id) {
        return new DataFile(new File(storageFolder, id + ".yml"), new StorageCellVersionUpdater());
    }

    /**
     * Checks whether or not readers can be used in the given world.
     *
     * @param world The world.
     * @return {@code true} iff readers can be used in the given world.
     */
    public boolean canUseReader(final World world) {
        return allowedReaderWorlds.contains(world.getName());
    }

    /**
     * Checks whether or not special drops can occur in the given world.
     *
     * @param world The world.
     * @return {@code true} iff special drops can occur in the given world.
     */
    public boolean canHaveDrops(final World world) {
        return allowedDropWorlds.contains(world.getName());
    }

    @Override
    public void onDisable() {
        cellManager.saveAll();
    }

    @Override
    public void onEnable() {
        self = this;

        // Register messages.
        Messenger.register("sacknaht.banned");
        Messenger.register("sacknaht.notinsync");
        Messenger.register("sacknaht.outofspace");
        Messenger.register("sacknaht.search");
        Messenger.register("sacknaht.cannotmanage");
        Messenger.register("sacknaht.cannotmanagemultiple");
        Messenger.register("sacknaht.showall");
        Messenger.register("sacknaht.manage");
        Messenger.register("sacknaht.quitManage");
        Messenger.register("sacknaht.userNotExists");

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        allowedReaderWorlds = new HashSet<>();
        allowedReaderWorlds.addAll(getConfig().getStringList("allowedWorlds"));

        allowedDropWorlds = new HashSet<>();
        allowedDropWorlds.addAll(getConfig().getStringList("dropWorlds"));

        // Create the data directory for the towns, if needed.
        storageFolder = new File(getDataFolder(), "data");
        if (!storageFolder.exists()) {
            if (!storageFolder.mkdir()) {
                throw new IllegalStateException("Could not create data directory: failure");
            }
        }

        // Create the cell manager.
        cellManager = new CellManager();
        searchManager = new SearchManager(this);
        interfaceManager = new InterfaceManager();
        memberManager = new MemberManager(this);

        // Register command executor modules.
        getCommand("me").setExecutor(new MECommandModule());

        // Register modules.
        getServer().getPluginManager().registerEvents(new BackpackOpenModule(), this);
        getServer().getPluginManager().registerEvents(new CellInitializerModule(), this);
        getServer().getPluginManager().registerEvents(new CraftingDropsModule(), this);
        getServer().getPluginManager().registerEvents(new MEInterfaceModule(), this);

        // Register multiblock structures.
        MachineRegistry.registerFactory(new ReaderFactory(), Material.ENCHANTMENT_TABLE);
        MachineRegistry.registerSingleBlockMachine(new AnvilMachine(), Material.ANVIL);
        MachineRegistry.registerSingleBlockMachine(new CauldronMachine(), Material.CAULDRON);
        MachineRegistry.registerSingleBlockMachine(new WorkbenchMachine(), Material.WORKBENCH);

        // Register periodic saving.
        getServer().getScheduler().scheduleSyncRepeatingTask(this, cellManager::save, 20L, 1200L);
    }
}
