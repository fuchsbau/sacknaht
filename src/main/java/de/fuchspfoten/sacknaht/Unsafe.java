package de.fuchspfoten.sacknaht;

import net.minecraft.server.v1_12_R1.NBTBase;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeSet;

/**
 * Unsafe operations.
 */
public final class Unsafe {

    /**
     * Whether or not meta classes were already checked for compatibility.
     */
    private static boolean checkedMetaClass = false;

    /**
     * Whether or not the backend used for itemmetas is invalid or not.
     */
    private static boolean validBackend = true;

    /**
     * The field "unhandledTags" of {@link org.bukkit.craftbukkit.v1_12_R1.inventory.CraftMetaItem}.
     */
    private static Field unhandledField;

    /**
     * Compares the internal unhandled tags of two item metas.
     *
     * @param meta1 The first item meta.
     * @param meta2 The second item meta.
     * @return The comparison result.
     */
    @SuppressWarnings("unchecked")
    public static int compareMetaInternals(final ItemMeta meta1, final ItemMeta meta2) {
        if (!checkedMetaClass) {
            if (meta1.getClass().getName().equals("org.bukkit.craftbukkit.v1_12_R1.inventory.CraftMetaItem")) {
                try {
                    unhandledField = meta1.getClass().getDeclaredField("unhandledTags");
                    unhandledField.setAccessible(true);
                } catch (final NoSuchFieldException e) {
                    throw new IllegalStateException(e);
                }
            } else {
                validBackend = false;
            }
            checkedMetaClass = true;
        }

        if (!validBackend) {
            return 0;
        }

        try {
            final Map<String, NBTBase> internal1 = (Map<String, NBTBase>) unhandledField.get(meta1);
            final Map<String, NBTBase> internal2 = (Map<String, NBTBase>) unhandledField.get(meta2);
            final Iterable<String> keys = new TreeSet<>(internal1.keySet());

            if (internal1.size() != internal2.size()) {
                return Integer.compare(internal1.size(), internal2.size());
            }

            for (final String key : keys) {
                final NBTBase nbt1 = internal1.get(key);
                final NBTBase nbt2 = internal2.get(key);

                if (nbt1 == null) {
                    if (nbt2 != null) {
                        return -1;
                    }

                    // Both are null.
                    continue;
                }

                if (nbt2 == null) {
                    return 1;
                }

                final String strRep1 = nbt1.toString();
                final String strRep2 = nbt2.toString();
                final int cmpStr = strRep1.compareTo(strRep2);
                if (cmpStr != 0) {
                    return cmpStr;
                }
            }

            return 0;
        } catch (final IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Unsafe() {
    }
}
