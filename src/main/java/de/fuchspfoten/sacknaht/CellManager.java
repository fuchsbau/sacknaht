package de.fuchspfoten.sacknaht;

import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.sacknaht.model.CellType;
import de.fuchspfoten.sacknaht.model.StorageCell;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Manages {@link StorageCell}s.
 */
public class CellManager {

    /**
     * The cell storage for {@link StorageCell}s.
     */
    private final Map<String, StorageCell> cellStorage = new HashMap<>();

    /**
     * The storage queue for cells to be saved.
     */
    private final Deque<StorageCell> saveQueue = new ArrayDeque<>();

    /**
     * Obtains an existing cell. If it does not exist, this method returns null.
     *
     * @param id The ID.
     * @return The cell.
     */
    public StorageCell getExistingCell(final String id) {
        if (cellStorage.containsKey(id)) {
            return cellStorage.get(id);
        }
        if (!SacknahtPlugin.getSelf().cellExists(id)) {
            return null;
        }
        final StorageCell sc = StorageCell.loadCell(SacknahtPlugin.getSelf().getCellDataFile(id));
        cellStorage.put(id, sc);
        saveQueue.addLast(sc);
        return sc;
    }

    /**
     * Obtains a new storage cell.
     *
     * @param owner         The owner of the cell.
     * @param limitSlots    The maximal number of slots this cell may contain.
     * @param limitSlotSize The maximal number of items a single slot may contain.
     * @param type The type of the cell.
     * @return The new storage cell.
     */
    public StorageCell getNewCell(final UUID owner, final int limitSlots, final int limitSlotSize,
                                  final CellType type) {
        String id = Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
        if (cellStorage.containsKey(id)) {
            id += "_";
        }

        final StorageCell sc = StorageCell.createCell(id, owner, limitSlots, limitSlotSize, type);

        // Store the ID in the player's data.
        final PlayerData data = new PlayerData(owner);
        final List<String> storedIds = data.getStorage().getStringList("sacknaht.owned");
        storedIds.add(id);
        data.getStorage().set("sacknaht.owned", storedIds);

        cellStorage.put(id, sc);
        saveQueue.addLast(sc);
        return sc;
    }

    /**
     * Saves 10 cells.
     */
    public void save() {
        if (saveQueue.isEmpty()) {
            return;
        }

        // Save 10 cells or save for 10ms, whatever is reached first.
        final long startTime = System.currentTimeMillis();
        final int k = Math.min(saveQueue.size(), 10);
        for (int i = 0; i < k; i++) {
            final StorageCell next = saveQueue.element();

            // Save and add to the end.
            next.saveCell();
            saveQueue.addLast(next);

            if (System.currentTimeMillis() - startTime >= 10) {
                break;
            }
        }
    }

    /**
     * Saves all cells.
     */
    public void saveAll() {
        for (final StorageCell cell : cellStorage.values()) {
            cell.saveCell();
        }
    }
}
