package de.fuchspfoten.sacknaht.search;

import de.fuchspfoten.sacknaht.SacknahtPlugin;
import org.bukkit.enchantments.Enchantment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * A {@link QGramIndex} for {@link Enchantment}s.
 */
public class EnchantmentIndex extends QGramIndex<Enchantment> {

    /**
     * Constructor.
     */
    public EnchantmentIndex() {
        super(3);
    }

    /**
     * Loads all provided materials into the index.
     */
    public void loadAll() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(
                SacknahtPlugin.getSelf().getResource("enchantments.txt")))) {
            while (true) {
                final String line = reader.readLine();
                if (line == null) {
                    break;
                }

                final String trimLine = line.trim();
                if (trimLine.isEmpty()) {
                    continue;
                }

                final String[] keyvalPair = trimLine.split("=");
                final Enchantment target = Enchantment.getByName(keyvalPair[0]);
                final String[] keywords = keyvalPair[1].split(" ");
                for (final String keyword : keywords) {
                    addDocument(keyword, target);
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
