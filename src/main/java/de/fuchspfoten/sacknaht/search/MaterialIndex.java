package de.fuchspfoten.sacknaht.search;

import de.fuchspfoten.sacknaht.SacknahtPlugin;
import org.bukkit.Material;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * A {@link QGramIndex} for {@link Material}s.
 */
public class MaterialIndex extends QGramIndex<Material> {

    /**
     * Constructor.
     */
    public MaterialIndex() {
        super(3);
    }

    /**
     * Loads all provided materials into the index.
     */
    public void loadAll() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(
                SacknahtPlugin.getSelf().getResource("materials.txt")))) {
            while (true) {
                final String line = reader.readLine();
                if (line == null) {
                    break;
                }

                final String trimLine = line.trim();
                if (trimLine.isEmpty()) {
                    continue;
                }

                final String[] keyvalPair = trimLine.split("=");
                final Material target = Material.valueOf(keyvalPair[0]);
                final String[] keywords = keyvalPair[1].split(" ");
                for (final String keyword : keywords) {
                    addDocument(keyword, target);
                }
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
