package de.fuchspfoten.sacknaht.search;

import de.fuchspfoten.fuchslib.util.Pair;
import de.fuchspfoten.fuchslib.util.primitive.ValueMutableIntPair;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Implements a q-Gram index.
 *
 * @param <T> The document type stored in the index.
 */
@RequiredArgsConstructor
public class QGramIndex<T> {

    private static final Pattern SANITIZE_REMOVE_PATTERN = Pattern.compile("\\W",
            Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * The value for the parameter Q.
     */
    private final int q;

    /**
     * The list of documents.
     */
    private final List<Pair<String, T>> documents = new ArrayList<>();

    /**
     * The inverted index mapping Q-Grams to document IDs.
     */
    private final Map<String, List<Integer>> index = new HashMap<>();

    /**
     * Calculates the edit distance of the two strings.
     *
     * @param a The first string.
     * @param b The second string.
     * @return The edit distance.
     */
    private static int editDistance(final CharSequence a, final CharSequence b) {
        final int n = a.length() + 1;
        final int m = b.length() + 1;
        final int[][] matrix = new int[n][m];

        // Initialize the matrix.
        for (int i = 0; i < n; i++) {
            matrix[i][0] = i;
        }
        for (int i = 0; i < m; i++) {
            matrix[0][i] = i;
        }

        // Fill the matrix.
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                final int changeCost = (a.charAt(i - 1) == b.charAt(j - 1)) ? 0 : 1;
                matrix[i][j] = Math.min(matrix[i - 1][j - 1] + changeCost,
                        Math.min(matrix[i][j - 1] + 1, matrix[i - 1][j] + 1));
            }
        }

        return matrix[n - 1][m - 1];
    }

    /**
     * Merges the given sorted lists of value mutable int pairs. The merging adds the values of the pairs merged
     * together.
     *
     * @param lists The sorted lists.
     * @return The resulting list.
     */
    private static List<ValueMutableIntPair> mergeSortedLists(final List<List<ValueMutableIntPair>> lists) {
        final List<ValueMutableIntPair> result = new ArrayList<>();
        final int[] pointers = new int[lists.size()];
        final Queue<Pair<ValueMutableIntPair, Integer>> queue =
                new PriorityQueue<>(Comparator.comparingInt(p -> p.getKey().getKey()));

        // Add initial elements.
        for (int i = 0; i < lists.size(); i++) {
            final List<ValueMutableIntPair> list = lists.get(i);
            if (!list.isEmpty()) {
                queue.add(new Pair<>(list.get(0), i));
                pointers[i]++;
            }
        }

        // Add all pairs in sorted order to the result.
        ValueMutableIntPair previous = null;
        while (!queue.isEmpty()) {
            final Pair<ValueMutableIntPair, Integer> next = queue.poll();
            if (previous == null || previous.getKey() != next.getKey().getKey()) {
                // Unseen ID.
                previous = next.getKey();
                result.add(previous);
            } else {
                // Previously seen ID.
                previous.setValue(previous.getValue() + next.getKey().getValue());
            }

            // Add the next element from the list where we just removed from.
            final int sourceIndex = next.getValue();
            final List<ValueMutableIntPair> sourceList = lists.get(sourceIndex);
            if (pointers[sourceIndex] < sourceList.size()) {
                queue.add(new Pair<>(sourceList.get(pointers[sourceIndex]), sourceIndex));
                pointers[sourceIndex]++;
            }
        }

        return result;
    }

    /**
     * Sanitizes the given key: converts it to lower case and removes non-word characters.
     *
     * @param key The key.
     * @return The sanitized key.
     */
    private static String sanitizeKey(final CharSequence key) {
        return SANITIZE_REMOVE_PATTERN.matcher(key).replaceAll("").toLowerCase();
    }

    /**
     * Adds a document to the index.
     *
     * @param key The document key.
     * @param doc The document.
     */
    public void addDocument(final String key, final T doc) {
        final int docId = documents.size();
        documents.add(new Pair<>(key, doc));

        for (final String qGram : getQGrams(key)) {
            index.computeIfAbsent(qGram, x -> new ArrayList<>()).add(docId);
        }
    }

    /**
     * Finds the results for multiple keywords, sorted ascending by cumulative edit distance.
     *
     * @param query       The keywords.
     * @param maxDistance The maximum edit distance to be included.
     * @return The results.
     */
    public List<Pair<T, Integer>> fuzzyFind(final Collection<String> query, final int maxDistance) {
        final Map<T, Integer> minMap = new HashMap<>();
        final List<Pair<Pair<String, T>, Integer>> results = find(query, maxDistance);
        for (final Pair<Pair<String, T>, Integer> entry : results) {
            final Integer old = minMap.get(entry.getKey().getValue());
            if (old == null) {
                minMap.put(entry.getKey().getValue(), entry.getValue());
            } else if (old > entry.getValue()) {
                minMap.put(entry.getKey().getValue(), entry.getValue());
            }
        }

        final List<Pair<T, Integer>> entries = new ArrayList<>();
        for (final Entry<T, Integer> entry : minMap.entrySet()) {
            entries.add(new Pair<>(entry.getKey(), entry.getValue()));
        }
        entries.sort(Comparator.comparingInt(Pair::getValue));
        return entries;
    }

    /**
     * Converts the given list of documents with counts to edit-distance-annotated documents.
     *
     * @param documentCounts The document counts that are converted.
     * @param query          The query string.
     * @param maxDistance    The maximum edit distance that is allowed.
     * @return The converted list.
     */
    private List<ValueMutableIntPair> convertEditDistance(final Collection<ValueMutableIntPair> documentCounts,
                                                          final CharSequence query, final int maxDistance) {
        final String sanitizedQuery = sanitizeKey(query);
        final int reducer = 1 + (maxDistance - 1) * q;
        return documentCounts.stream()
                .filter(ic -> {
                    final String docKey = documents.get(ic.getKey()).getKey();
                    final int maxSize = Math.max(sanitizedQuery.length(), docKey.length());
                    return ic.getValue() >= maxSize - reducer;
                })
                .map(ic -> {
                    final String docKey = documents.get(ic.getKey()).getKey();
                    return new ValueMutableIntPair(ic.getKey(), editDistance(sanitizedQuery, docKey));
                })
                .filter(ie -> ie.getValue() <= maxDistance)
                .collect(Collectors.toList());
    }

    /**
     * Fetches the document lists for the given query.
     *
     * @param query The query.
     * @return The document lists for the query.
     */
    private List<List<Integer>> fetchDocumentLists(final CharSequence query) {
        return Arrays.stream(getQGrams(query))
                .map(index::get)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Finds the results for multiple keywords, sorted ascending by cumulative edit distance.
     *
     * @param query       The keywords.
     * @param maxDistance The maximum edit distance to be included.
     * @return The results.
     */
    private List<Pair<Pair<String, T>, Integer>> find(final Collection<String> query, final int maxDistance) {
        final List<List<ValueMutableIntPair>> singleResults = query.stream()
                .map(s -> findSingle(s, maxDistance))
                .collect(Collectors.toList());
        final List<ValueMutableIntPair> merged = mergeSortedLists(singleResults);
        merged.sort(Comparator.comparingInt(ValueMutableIntPair::getValue));
        return merged.stream()
                .map(p -> new Pair<>(documents.get(p.getKey()), p.getValue()))
                .collect(Collectors.toList());
    }

    /**
     * Finds the results for a single keyword.
     *
     * @param query       The keyword.
     * @param maxDistance The maximum edit distance to be included.
     * @return The results.
     */
    private List<ValueMutableIntPair> findSingle(final CharSequence query, final int maxDistance) {
        final List<List<Integer>> docLists = fetchDocumentLists(query);
        final List<List<ValueMutableIntPair>> documentCountInit = docLists.stream()
                .map(l -> l.stream()
                        .map(i -> new ValueMutableIntPair(i, 1))
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        final List<ValueMutableIntPair> documentCounts = mergeSortedLists(documentCountInit);
        return convertEditDistance(documentCounts, query, maxDistance);
    }

    /**
     * Retrieves the q-Grams of the given string.
     *
     * @param key The input string.
     * @return The q-Grams.
     */
    private String[] getQGrams(final CharSequence key) {
        final String sanitizedKey = sanitizeKey(key);
        if (sanitizedKey.isEmpty()) {
            return new String[0];
        }

        // The number of padded q-Grams is |in| + q - 1.
        final String pad = String.join("", Collections.nCopies(q - 1, "\0"));
        final String padded = pad + sanitizedKey + pad;
        final String[] result = new String[sanitizedKey.length() + q - 1];
        Arrays.setAll(result, i -> padded.substring(i, i + q));
        return result;
    }
}
