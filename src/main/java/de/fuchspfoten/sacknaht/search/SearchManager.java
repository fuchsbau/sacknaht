package de.fuchspfoten.sacknaht.search;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.util.Pair;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.model.StorageCell;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Manages searches of users.
 */
public class SearchManager {

    /**
     * The conversation factory.
     */
    private final ConversationFactory conversationFactory;

    /**
     * The material index.
     */
    private final MaterialIndex matIndex;

    /**
     * The enchantment index.
     */
    private final EnchantmentIndex enchIndex;

    /**
     * Constructor.
     */
    public SearchManager(final JavaPlugin owner) {
        conversationFactory = new ConversationFactory(owner).withModality(false).withLocalEcho(false);
        matIndex = new MaterialIndex();
        matIndex.loadAll();
        enchIndex = new EnchantmentIndex();
        enchIndex.loadAll();
    }

    /**
     * Opens a search prompt for the given player in the given data set.
     *
     * @param target  The player.
     * @param dataset The data set.
     */
    public void openPrompt(final Conversable target, final Collection<StorageCell> dataset) {
        conversationFactory.withFirstPrompt(new SearchPrompt(dataset)).buildConversation(target).begin();
    }

    /**
     * A prompt for searching in a cell set.
     */
    @RequiredArgsConstructor
    private class SearchPrompt extends StringPrompt {

        /**
         * The data set in which to search.
         */
        private final Collection<StorageCell> dataset;

        @Override
        public Prompt acceptInput(final ConversationContext context, final String input) {
            final List<String> query = Arrays.asList(input.split(" "));
            final List<Pair<Material, Integer>> materialsFound = new ArrayList<>();
            final List<Pair<Enchantment, Integer>> enchantmentsFound = new ArrayList<>();

            // Perform the search in the indexes.
            int allowedError = 2;
            while (materialsFound.isEmpty() && enchantmentsFound.isEmpty()) {
                materialsFound.addAll(matIndex.fuzzyFind(query, allowedError));
                enchantmentsFound.addAll(enchIndex.fuzzyFind(query, allowedError));
                allowedError *= 2;

                if (allowedError > 8) {
                    break;
                }
            }

            // Find the highest ranked results.
            final Set<Material> materials = EnumSet.noneOf(Material.class);
            final Collection<Enchantment> enchantments = new HashSet<>();
            final int rankMaterial = materialsFound.isEmpty() ? Integer.MAX_VALUE : materialsFound.get(0).getValue();
            final int rankEnchantment =
                    enchantmentsFound.isEmpty() ? Integer.MAX_VALUE : enchantmentsFound.get(0).getValue();
            if (rankMaterial <= rankEnchantment) {
                for (final Pair<Material, Integer> hit : materialsFound) {
                    if (hit.getValue() != rankMaterial) {
                        break;
                    }
                    materials.add(hit.getKey());
                }
            }
            if (rankEnchantment <= rankMaterial) {
                for (final Pair<Enchantment, Integer> hit : enchantmentsFound) {
                    if (hit.getValue() != rankEnchantment) {
                        break;
                    }
                    enchantments.add(hit.getKey());
                }
            }

            // Create the predicate from the results.
            final Predicate<ItemPattern> searchPredicate =
                    p -> materials.contains(p.getType()) || enchantments.stream().anyMatch(p::hasEnchant);

            // Open the view using the results.
            SacknahtPlugin.getSelf().getInterfaceManager().open(dataset, (Entity) context.getForWhom(),
                    searchPredicate);

            return Prompt.END_OF_CONVERSATION;
        }

        @Override
        public String getPromptText(final ConversationContext context) {
            return Messenger.getFormat("sacknaht.search");
        }

        @Override
        public SearchPrompt clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }
    }
}
