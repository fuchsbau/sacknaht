package de.fuchspfoten.sacknaht.ui;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.sacknaht.model.ActionOutcome;
import de.fuchspfoten.sacknaht.model.ActionOutcome.ActionResult;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;

/**
 * Manages the {@link MEInterface}s in existance.
 */
public class InterfaceManager {

    /**
     * Maps players to opened ME interfaces.
     */
    private final Map<UUID, MEInterface> interfaceMap = new HashMap<>();

    /**
     * Attempts to add the given stack to the storage. Returns true on success.
     *
     * @param item   The item.
     * @param viewer The viewer.
     * @return Whether the action was successful.
     */
    public boolean addStack(final ItemStack item, final Entity viewer) {
        final MEInterface meInterface = interfaceMap.get(viewer.getUniqueId());
        final ActionOutcome result = meInterface.addStack(item);

        if (result.getResult() == ActionResult.OUT_OF_SPACE) {
            Messenger.send(viewer, "sacknaht.outofspace");
        }
        if (result.getResult() == ActionResult.BANNED) {
            Messenger.send(viewer, "sacknaht.banned");
        }

        return result.getResult() == ActionResult.OK;
    }

    /**
     * Checks whether or not the given UUID is using the ME interface.
     *
     * @param who The UUID.
     * @return Whether or not the given UUID is using the ME interface.
     */
    public boolean isInInterface(final UUID who) {
        return interfaceMap.containsKey(who);
    }

    /**
     * Called when the given player closes an interface.
     *
     * @param who The player.
     */
    public void onClose(final UUID who) {
        interfaceMap.remove(who);
    }

    /**
     * Opens an interface for the given data set for the given player.
     *
     * @param dataset The dataset.
     * @param viewer  The player.
     */
    public void open(final Collection<StorageCell> dataset, final Entity viewer) {
        open(dataset, viewer, null);
    }

    /**
     * Opens an interface for the given data set for the given player.
     *
     * @param dataset The dataset.
     * @param viewer  The player.
     * @param filter  The filtering predicate.
     */
    public void open(final Collection<StorageCell> dataset, final Entity viewer,
                     final Predicate<ItemPattern> filter) {
        interfaceMap.put(viewer.getUniqueId(), new MEInterface(viewer.getUniqueId(), dataset, filter));
    }

    /**
     * Attempts to take half a stack from the storage. Returns the taken item (null on failure).
     *
     * @param escaped The item that is requested (escaped).
     * @param viewer  The viwer.
     * @return The taken item.
     */
    public ItemStack takeHalfStack(final ItemStack escaped, final Entity viewer) {
        return takeStack(escaped, viewer, 0.5f);
    }

    /**
     * Attempts to take a stack from the storage. Returns the taken item (null on failure).
     *
     * @param escaped The item that is requested (escaped).
     * @param viewer  The viwer.
     * @return The taken item.
     */
    public ItemStack takeStack(final ItemStack escaped, final Entity viewer) {
        return takeStack(escaped, viewer, 1.0f);
    }

    /**
     * Attempts to click the given toolbar slot.
     *
     * @param slot   The slot.
     * @param viewer The viewer.
     */
    public void toolbarClick(final int slot, final Entity viewer) {
        final MEInterface meInterface = interfaceMap.get(viewer.getUniqueId());
        meInterface.toolbarClick(slot);
    }

    /**
     * Attempts to take a stack from the storage. Returns the taken item (null on failure).
     *
     * @param escaped The item that is requested (escaped).
     * @param viewer  The viwer.
     * @param factor  The factor with which the amount that is to be taken is multiplied.
     * @return The taken item.
     */
    private ItemStack takeStack(final ItemStack escaped, final Entity viewer, final float factor) {
        final MEInterface meInterface = interfaceMap.get(viewer.getUniqueId());
        final ActionOutcome outcome = meInterface.takeStack(escaped, factor);

        if (outcome.getResult() == ActionResult.NOT_IN_SYNC) {
            Messenger.send(viewer, "sacknaht.notinsync");
        }

        return outcome.getOutcome();
    }
}
