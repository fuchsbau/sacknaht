package de.fuchspfoten.sacknaht.ui;

import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.item.CustomItems;
import de.fuchspfoten.fuchslib.item.ItemFactory;
import de.fuchspfoten.sacknaht.SacknahtPlugin;
import de.fuchspfoten.sacknaht.model.ActionOutcome;
import de.fuchspfoten.sacknaht.model.ActionOutcome.ActionResult;
import de.fuchspfoten.sacknaht.model.CellView;
import de.fuchspfoten.sacknaht.model.ItemPattern;
import de.fuchspfoten.sacknaht.model.StorageCell;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import java.util.function.Predicate;

/**
 * Represents an ME interface.
 */
public final class MEInterface {

    /**
     * The number of payload items per page, exactly 5 rows `a 9 items.
     */
    private static final int ITEMS_PER_PAGE = 9 * 5;

    /**
     * The data set on which the interface is based.
     */
    private final Collection<StorageCell> dataSet;

    /**
     * The backend cell view for the interface.
     */
    private final CellView view;

    /**
     * The viewer of the interface.
     */
    private final UUID viewer;

    /**
     * The inventory used as a UI.
     */
    private final Inventory inventory;

    /**
     * The page that is currently being viewed.
     */
    private int page = 1;

    /**
     * Constructor.
     *
     * @param viewer  The viewer for the interface.
     * @param dataSet The backend data set for the interface.
     * @param filter The filtering predicate.
     */
    public MEInterface(final UUID viewer, final Collection<StorageCell> dataSet, final Predicate<ItemPattern> filter) {
        final Player who = Bukkit.getPlayer(viewer);
        if (who == null || !who.isOnline()) {
            throw new IllegalArgumentException("Invalid UUID: Not online");
        }

        this.dataSet = new ArrayList<>(dataSet);
        view = new CellView(dataSet, filter);
        this.viewer = viewer;
        inventory = Bukkit.createInventory(null, 54, "");
        updateInventory();
        who.openInventory(inventory);
    }

    /**
     * Attempts to add the given stack to the storage.
     *
     * @param item The item.
     * @return The result of the action
     */
    public ActionOutcome addStack(final ItemStack item) {
        final ActionOutcome result = view.addItem(new ItemPattern(item), item.getAmount());
        updateInventory();
        return result;
    }

    /**
     * Attempts to take a stack from the storage. Returns the outcome of the action.
     *
     * @param escaped The item that is requested (escaped).
     * @param factor The factor with which the max take amount is multiplied.
     * @return The outcome of the action.
     */
    public ActionOutcome takeStack(final ItemStack escaped, final float factor) {
        final ItemPattern pattern = ItemPattern.unescapeItem(escaped);
        final long amount = view.getAmountOf(pattern);
        if (amount <= 0) {
            return new ActionOutcome(ActionResult.NOT_IN_SYNC, null);
        }

        final long maxTake = Math.min(amount, escaped.getMaxStackSize());
        final long toTake = Math.max(1, (int) (maxTake * factor));
        final long taken = view.take(pattern, toTake);
        updateInventory();

        final ItemStack result = pattern.getSample();
        result.setAmount((int) taken);
        return new ActionOutcome(ActionResult.OK, result);
    }

    /**
     * Attempts to click the given toolbar slot.
     *
     * @param slot The slot.
     */
    public void toolbarClick(final int slot) {
        final int pages = view.getUsedSlots() / ITEMS_PER_PAGE
                + ((view.getUsedSlots() % ITEMS_PER_PAGE == 0) ? 0 : 1);

        if (slot == 47 && page > 1) {
            page--;
            updateInventory();
        } else if (slot == 51 && page < pages) {
            page++;
            updateInventory();
        } else if (slot == 53) {
            final Runnable task = () -> {
                final Player who = Bukkit.getPlayer(viewer);
                if (who != null && who.isOnline()) {
                    who.closeInventory();
                    SacknahtPlugin.getSelf().getSearchManager().openPrompt(who, dataSet);
                }
            };
            SacknahtPlugin.getSelf().getServer().getScheduler().scheduleSyncDelayedTask(SacknahtPlugin.getSelf(), task,
                    1L);
        } else if (slot == 45) {
            final Player viewerPlayer = Bukkit.getPlayer(viewer);
            if (viewerPlayer != null && viewerPlayer.isOnline()) {
                if (dataSet.size() == 1) {
                    final StorageCell cell = dataSet.stream().findAny().orElse(null);
                    if (cell.canManage(viewerPlayer)) {
                        if (!viewerPlayer.isConversing()) {
                            final Runnable task = () -> {
                                final Player who = Bukkit.getPlayer(viewer);
                                if (who != null && who.isOnline()) {
                                    who.closeInventory();
                                    SacknahtPlugin.getSelf().getMemberManager().openPrompt(who, cell);
                                }
                            };
                            SacknahtPlugin.getSelf().getServer().getScheduler().scheduleSyncDelayedTask(
                                    SacknahtPlugin.getSelf(), task, 1L);
                        }
                    } else {
                        Messenger.send(viewerPlayer, "sacknaht.cannotmanage");
                    }
                } else {
                    Messenger.send(viewerPlayer, "sacknaht.cannotmanagemultiple");
                }
            }
        }
    }

    /**
     * Creates the inventory from scratch.
     */
    private void updateInventory() {
        final int pages = view.getUsedSlots() / ITEMS_PER_PAGE
                + ((view.getUsedSlots() % ITEMS_PER_PAGE == 0) ? 0 : 1);

        // 45..53: Control bar.
        final ItemFactory accessControl = CustomItems.LOCK_PLUS.getFactory()
                .name("§3Zugriff verwalten")
                .ban();
        inventory.setItem(45, accessControl.instance());

        if (page > 1) {
            final ItemFactory pgLeft = CustomItems.ARROW_LEFT.getFactory()
                    .name("§3Seite " + (page - 1))
                    .ban();
            inventory.setItem(47, pgLeft.instance());
        } else {
            inventory.setItem(47, null);
        }

        if (page < pages) {
            final ItemFactory pgRight = CustomItems.ARROW_RIGHT.getFactory()
                    .name("§3Seite " + (page + 1))
                    .ban();
            inventory.setItem(51, pgRight.instance());
        } else {
            inventory.setItem(51, null);
        }

        final ItemFactory info = CustomItems.INFORMATION_SYMBOL.getFactory()
                .name("§3Information")
                .lore("§eSlots: §5" + view.getRealUsedSlots() + "§e/§5" + view.getMaxSlots())
                .lore("§eSlotgröße: §5" + view.getMaxItemsPerSlot())
                .ban();
        inventory.setItem(49, info.instance());

        final ItemFactory search = CustomItems.MAGNIFYING_GLASS.getFactory()
                .name("§3Suchen")
                .ban();
        inventory.setItem(53, search.instance());

        updateStorage();
    }

    /**
     * Updates the storage area.
     */
    private void updateStorage() {
        final int skip = ITEMS_PER_PAGE * (page - 1);
        int slotPtr = 0;
        for (final ItemStack entry : view.getContentPart(ITEMS_PER_PAGE, skip)) {
            inventory.setItem(slotPtr++, entry);
        }
        while (slotPtr < 45) {
            inventory.setItem(slotPtr++, null);
        }
    }
}
